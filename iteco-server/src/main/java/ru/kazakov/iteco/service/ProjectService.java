package ru.kazakov.iteco.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kazakov.iteco.api.repository.IProjectRepository;
import ru.kazakov.iteco.api.service.IProjectService;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.entity.Session;
import ru.kazakov.iteco.enumeration.SortType;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    @Autowired
    private IProjectRepository repository;

    @Override
    public void persist(@Nullable final Project entity) throws Exception {
        if (entity == null) throw new Exception();
        repository.persistProject(entity);
    }

    @Override
    public void merge(@Nullable final Project entity) throws Exception {
        if (entity == null) throw new Exception();
        repository.mergeProject(entity);
    }

    @Override
    public void create(
            @Nullable final Session session,
            @Nullable final String name
    ) throws Exception {
        if (session == null) throw new Exception();
        if (name == null || name.isEmpty()) throw new Exception();
        @NotNull final Project project = new Project();
        project.setUser(session.getUser());
        project.setName(name);
        persist(project);
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        repository.removeProject(id);
    }

    @Override
    public void removeAll() throws Exception {
        repository.removeAllProjects();
    }

    @Override
    public void removeAll(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        repository.removeAllProjectsByCurrentId(currentUserId);
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @Nullable final Project project = repository.findOneProject(id);
        return project;
    }

    @Nullable
    @Override
    public Project findByName(
            @Nullable final String currentUserId,
            @Nullable final String name
    ) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final Project project = repository.findByProjectNameCurrentId(currentUserId, name);
        return project;
    }

    @NotNull
    @Override
    public List<Project> findAll() throws Exception {
        @Nullable final List<Project> list = repository.findAllProjects();
        return list == null ? Collections.emptyList() : list;
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final List<Project> list = repository.findAllProjectsByCurrentId(currentUserId);
        return list == null ? Collections.emptyList() : list;
    }

    @NotNull
    @Override
    public List<String> findAll(
            @Nullable final String currentUserId,
            @Nullable final SortType sortType
    ) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        if (sortType == null) throw new Exception();
        @Nullable final List<String> list = repository.findAllSortedProjectsByCurrentId(currentUserId, sortType);
        return list == null ? Collections.emptyList() : list;
    }

    @NotNull
    @Override
    public List<String> findAllByName(
            @Nullable final String currentUserId,
            @Nullable final String part
    ) throws Exception {
        if (part == null || part.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final List<String> list = repository.findAllProjectsByNameCurrentId(currentUserId, part);
        return list == null ? Collections.emptyList() : list;
    }

    @NotNull
    @Override
    public List<String> findAllByInfo(
            @Nullable final String currentUserId,
            @Nullable final String part
    ) throws Exception {
        if (part == null || part.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final List<String> list = repository.findAllProjectsByInfoCurrentId(currentUserId, part);
        return list == null ? Collections.emptyList() : list;
    }

    @Override
    public boolean contains(@Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        final boolean contains = repository.containsProjectByName(name);
        return contains;
    }

    @Override
    public boolean contains(
            @Nullable final String currentUserId,
            @Nullable final String name
    ) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        final boolean contains = repository.containsProjectByNameCurrentId(currentUserId, name);
        return contains;
    }

    @Override
    public boolean isEmptyRepository(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        final boolean isEmpty = repository.isEmptyProjectRepository(currentUserId);
        return isEmpty;
    }

}
