package ru.kazakov.iteco.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kazakov.iteco.api.repository.IUserRepository;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.entity.User;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    @Autowired
    private IUserRepository repository;

    @Override
    public void persist(@Nullable final User entity) throws Exception {
        if (entity == null) throw new Exception();
        repository.persistUser(entity);
    }

    @Override
    public void merge(@Nullable final User entity) throws Exception {
        if (entity == null) throw new Exception();
        repository.mergeUser(entity);
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        repository.removeUser(id);
    }

    @Override
    public void removeAll() {
        repository.removeAllUsers();
    }

    @Nullable
    @Override
    public User findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @Nullable final User user = repository.findOneUser(id);
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception();
        @Nullable final User user = repository.findUserByLogin(login);
        return user;
    }

    @Nullable
    @Override
    public User findCurrentUser(@Nullable final String currentId) throws Exception {
        if (currentId == null || currentId.isEmpty()) throw new Exception();
        return findOne(currentId);
    }

    @NotNull
    @Override
    public List<User> findAll() {
        @NotNull final List<User> list = repository.findAllUsers();
        return list == null ? Collections.emptyList() : list;
    }

    @NotNull
    @Override
    public List<String> findAllUsersLogins() throws Exception {
        @NotNull final List<String> list = repository.findAllUsersLogins();
        return list == null ? Collections.emptyList() : list;
    }

    @Override
    public boolean contains(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception();
        final boolean contains = repository.containsUserByLogin(login);
        return contains;
    }

}
