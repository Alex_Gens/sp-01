package ru.kazakov.iteco.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.kazakov.iteco.api.service.IService;
import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public abstract class AbstractService<T> implements IService<T> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @Override
    public abstract void persist(@Nullable final T entity) throws Exception;

    @Override
    public abstract void remove(@Nullable final String id) throws Exception;

    @Override
    public abstract void removeAll() throws Exception;

    @Nullable
    @Override
    public abstract T findOne(@Nullable final String id) throws Exception;

    @NotNull
    @Override
    public abstract List<T> findAll() throws Exception;

}
