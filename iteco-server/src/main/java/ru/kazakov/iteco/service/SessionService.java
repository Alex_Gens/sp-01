package ru.kazakov.iteco.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.kazakov.iteco.api.repository.ISessionRepository;
import ru.kazakov.iteco.api.service.ISessionService;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.dto.SessionDTO;
import ru.kazakov.iteco.entity.Session;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.util.AES;
import ru.kazakov.iteco.util.Password;
import ru.kazakov.iteco.util.SignatureUtil;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class SessionService extends AbstractService<Session> implements ISessionService {
    
    @NotNull
    @Autowired
    private ISessionRepository repository;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Value("${secret}")
    private String secret;

    @NotNull
    @Override
    public RoleType getRoleType(@Nullable final String token) throws Exception {
        if (token == null || token.isEmpty()) throw new Exception();
        @NotNull final String decryptToken = AES.decrypt(token, secret);
        @NotNull final ObjectMapper mapper = new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @Nullable final Session session = mapper.readValue(decryptToken, Session.class);
        if (session == null) throw new Exception();
        return session.getRoleType();
    }

    @Override
    public void persist(@Nullable final Session entity) throws Exception {
        if (entity == null) throw new Exception();
        repository.persistSession(entity);
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        repository.removeSession(id);
    }

    @Override
    public void removeByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        repository.removeSessionByUserId(userId);
    }

    @Override
    public void removeAll() {
        repository.removeAllSessions();
    }

    @Nullable
    @Override
    public Session findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        return repository.findOneSession(id);
    }

    @NotNull
    @Override
    public List<Session> findAll() {
        List<Session> list = repository.findAllSessions();
        return list == null ? Collections.emptyList() : list;
    }

    @Nullable
    @Override
    public String getInstance(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Session userSession,
            @Nullable final SessionDTO dto
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception();
        if (password == null || password.isEmpty()) throw new Exception();
        if (userSession == null) throw new Exception();
        if (dto == null) throw new Exception();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new Exception();
        if (user.getPassword() == null || user.getPassword().isEmpty()) throw new Exception();
        @NotNull final String enteredPassword = Password.getHashedPassword(password);
        if (user.getPassword().equals(enteredPassword)) {
            user.setDateStart(new Date(System.currentTimeMillis()));
            userService.merge(user);
            userSession.setRoleType(user.getRoleType());
            userSession.setUser(user);
            dto.setRoleType(userSession.getRoleType());
            dto.setUserId(userSession.getUser().getId());
            @Nullable final String signature = SignatureUtil.getSignature(dto);
            if (signature == null || signature.isEmpty()) throw new Exception();
            dto.setSignature(signature);
            userSession.setSignature(signature);
            @NotNull final ObjectMapper mapper = new ObjectMapper();
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
            @NotNull final String json = mapper.writeValueAsString(dto);
            @NotNull final String token = AES.encrypt(json, secret);
            persist(userSession);
            return token;
        }
        return null;
    }

    @Override
    public boolean contains(
            @Nullable final String userId,
            @Nullable final String id
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (id == null || id.isEmpty()) throw new Exception();
        final boolean contains = repository.containsSession(userId, id);
        return contains;
    }

}
