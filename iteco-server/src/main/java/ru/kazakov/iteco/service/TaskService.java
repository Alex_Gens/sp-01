package ru.kazakov.iteco.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kazakov.iteco.api.repository.ITaskRepository;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.entity.Session;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.SortType;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class TaskService extends AbstractService<Task> implements ITaskService {
    
    @NotNull
    @Autowired
    private ITaskRepository repository;

    @Override
    public void persist(@Nullable final Task entity) throws Exception {
        if (entity == null) throw new Exception();
        repository.persistTask(entity);
        
    }

    @Override
    public void merge(@Nullable final Task entity) throws Exception {
        if (entity == null) throw new Exception();
        repository.mergeTask(entity);
        
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        repository.removeTask(id);
        
    }

    @Override
    public void create(@Nullable final Session session,
                       @Nullable final String name
    ) throws Exception {
        if (session == null) throw new Exception();
        if (name == null || name.isEmpty()) throw new Exception();
        @NotNull final Task task = new Task();
        task.setUser(session.getUser());
        task.setName(name);
        persist(task);
    }

    @Override
    public void removeWithProject(
            @Nullable final String currentUserId,
            @Nullable final String projectId
    ) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        if (projectId == null || projectId.isEmpty()) throw new Exception();
        repository.removeTasksWithProject(currentUserId, projectId);
    }

    @Override
    public void removeAll() throws Exception {
        repository.removeAllTasks();
    }

    @Override
    public void removeAll(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        repository.removeAllTasksByCurrentId(currentUserId);
    }

    @Override
    public void removeAllWithProjects(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        repository.removeAllTasksWithProjects(currentUserId);
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @Nullable final Task task = repository.findOneTask(id);
        return task;
    }

    @Nullable
    @Override
    public Task findByName(
            @Nullable final String currentUserId,
            @Nullable final String name
    ) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final Task task = repository.findTaskByNameCurrentId(currentUserId, name);
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        @Nullable final List<Task> list = repository.findAllTasks();
        return list == null ? Collections.emptyList() : list;
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final List<Task> list = repository.findAllTasksByCurrentId(currentUserId);
        return list == null ? Collections.emptyList() : list;
    }

    @NotNull
    @Override
    public List<String> findAll(
            @Nullable final String currentUserId,
            @Nullable final SortType sortType
    ) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        if (sortType == null) throw new Exception();
        @NotNull final List<String> list = repository.findAllSortedTasksByCurrentId(currentUserId, sortType);
        return list == null ? Collections.emptyList() : list;
    }

    @NotNull
    @Override
    public List<String> findAll(
            @Nullable final String currentUserId,
            @Nullable final String projectId,
            @Nullable final SortType sortType) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        if (projectId == null || projectId.isEmpty()) throw new Exception();
        if (sortType == null) throw new Exception();
        @NotNull final List<String> list = repository.findAllSortedTasksByCurrentIdProjectId(
                currentUserId, projectId, sortType);
        return list == null ? Collections.emptyList() : list;
    }

    @NotNull
    @Override
    public List<String> findAllByName(
            @Nullable final String currentUserId,
            @Nullable final String part
    ) throws Exception {
        if (part == null || part.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @NotNull final List<String> list = repository.findAllTasksByNameCurrentId(currentUserId, part);
        return list == null ? Collections.emptyList() : list;
    }

    @NotNull
    @Override
    public List<String> findAllByInfo(
            @Nullable final String currentUserId,
            @Nullable final String part
    ) throws Exception {
        if (part == null || part.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @NotNull final List<String> list = repository.findAllTasksByInfoCurrentId(currentUserId, part);
        return list == null ? Collections.emptyList() : list;
    }

    @Override
    public boolean contains(@Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        final boolean contains = repository.containsTaskByName(name);
        return contains;
    }

    @Override
    public boolean contains(
            @Nullable final String currentUserId,
            @Nullable final String name
    ) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        final boolean contains = repository.containsTaskByNameCurrentId(currentUserId, name);
        return contains;
    }

    @Override
    public boolean isEmptyRepository(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        final boolean isEmpty = repository.isEmptyTaskRepository(currentUserId);
        return isEmpty;
    }

}
