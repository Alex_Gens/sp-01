package ru.kazakov.iteco.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kazakov.iteco.api.service.*;
import ru.kazakov.iteco.dto.*;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.entity.Session;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.entity.User;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

@Service
public final class DomainService implements IDomainService {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IUserService userService;

    @NotNull
    private final ISessionService sessionService;

    @Autowired
    public DomainService(@NotNull final IProjectService projectService,
                         @NotNull final ITaskService taskService,
                         @NotNull final IUserService userService,
                         @NotNull final ISessionService sessionService) {
        this.projectService = projectService;
        this.taskService = taskService;
        this.userService = userService;
        this.sessionService = sessionService;
    }

    public void saveBin(
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        if (fileName == null || fileName.isEmpty()) throw new Exception();
        if (directory == null || directory.isEmpty()) throw new Exception();
        @NotNull final Path dir = Paths.get(directory);
        @NotNull final Path path = Paths.get(directory + File.separator + fileName);
        if (!Files.isDirectory(dir)) Files.createDirectory(dir);
        Files.deleteIfExists(path);
        Files.createFile(path);
        try (@NotNull final ObjectOutput stream = new ObjectOutputStream(new FileOutputStream(path.toFile()))) {
            @NotNull final Domain domain = getInstance();
            stream.writeObject(domain);
            stream.flush();
            stream.close();
        } catch (Exception e) {e.printStackTrace();}
    }

    @Override
    public void saveJaxbXml(
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        if (fileName == null || fileName.isEmpty()) throw new Exception();
        if (directory == null || directory.isEmpty()) throw new Exception();
        @NotNull final Path dir = Paths.get(directory);
        @NotNull final Path path = Paths.get(directory + File.separator + fileName);
        if (!Files.isDirectory(dir)) Files.createDirectory(dir);
        Files.deleteIfExists(path);
        Files.createFile(path);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        @NotNull final Domain domain = getInstance();
        marshaller.marshal(domain, path.toFile());
    }

    @Override
    public void saveJaxbJson(
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        if (fileName == null || fileName.isEmpty()) throw new Exception();
        if (directory == null || directory.isEmpty()) throw new Exception();
        System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final Path dir = Paths.get(directory);
        @NotNull final Path path = Paths.get(directory + File.separator + fileName);
        if (!Files.isDirectory(dir)) Files.createDirectory(dir);
        Files.deleteIfExists(path);
        Files.createFile(path);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, Boolean.TRUE);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        @NotNull final Domain domain = getInstance();
        marshaller.marshal(domain, path.toFile());
    }

    @Override
    public void saveFasterXml(
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        if (fileName == null || fileName.isEmpty()) throw new Exception();
        if (directory == null || directory.isEmpty()) throw new Exception();
        @NotNull final Path dir = Paths.get(directory);
        @NotNull final Path path = Paths.get(directory + File.separator + fileName);
        if (!Files.isDirectory(dir)) Files.createDirectory(dir);
        Files.deleteIfExists(path);
        Files.createFile(path);
        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        @NotNull final Domain domain = getInstance();
        mapper.writeValue(path.toFile(), domain);
    }

    @Override
    public void saveFasterJson(
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        if (fileName == null || fileName.isEmpty()) throw new Exception();
        if (directory == null || directory.isEmpty()) throw new Exception();
        @NotNull final Path dir = Paths.get(directory);
        @NotNull final Path path = Paths.get(directory + File.separator + fileName);
        if (!Files.isDirectory(dir)) Files.createDirectory(dir);
        Files.deleteIfExists(path);
        Files.createFile(path);
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        @NotNull final Domain domain = getInstance();
        mapper.writeValue(path.toFile(), domain);
    }

    @Override
    public void loadBin(
            @Nullable final String userSessionId,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        if (userSessionId == null || userSessionId.isEmpty()) throw new Exception();
        if (fileName == null || fileName.isEmpty()) throw new Exception();
        if (directory == null || directory.isEmpty()) throw new Exception();
        @NotNull final Path path = Paths.get(directory + File.separator + fileName);
        try (ObjectInputStream stream = new ObjectInputStream(new FileInputStream(path.toFile()))) {
            @NotNull final Domain domain = (Domain) stream.readObject();
            stream.close();
            sessionService.remove(userSessionId);
            load(domain);
        } catch (Exception e) {e.printStackTrace();}
    }

    @Override
    public void loadJaxbXml(
            @Nullable final String userSessionId,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        if (userSessionId == null || userSessionId.isEmpty()) throw new Exception();
        if (fileName == null || fileName.isEmpty()) throw new Exception();
        if (directory == null || directory.isEmpty()) throw new Exception();
        @NotNull final Path path = Paths.get(directory + File.separator + fileName);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(path.toFile());
        sessionService.remove(userSessionId);
        load(domain);
    }

    @Override
    public void loadJaxbJson(
            @Nullable final String userSessionId,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        if (userSessionId == null || userSessionId.isEmpty()) throw new Exception();
        if (fileName == null || fileName.isEmpty()) throw new Exception();
        if (directory == null || directory.isEmpty()) throw new Exception();
        System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final Path path = Paths.get(directory + File.separator + fileName);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(path.toFile());
        sessionService.remove(userSessionId);
        load(domain);
    }

    @Override
    public void loadFasterXml(
            @Nullable final String userSessionId,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        if (userSessionId == null || userSessionId.isEmpty()) throw new Exception();
        if (fileName == null || fileName.isEmpty()) throw new Exception();
        if (directory == null || directory.isEmpty()) throw new Exception();
        @NotNull final Path path = Paths.get(directory + File.separator + fileName);
        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @NotNull final Domain domain = mapper.readValue(path.toFile(), Domain.class);
        sessionService.remove(userSessionId);
        load(domain);
    }

    @Override
    public void loadFasterJson(
            @Nullable final String userSessionId,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception {
        if (userSessionId == null || userSessionId.isEmpty()) throw new Exception();
        if (directory == null || directory.isEmpty()) throw new Exception();
        if (fileName == null || fileName.isEmpty()) throw new Exception();
        @NotNull final Path path = Paths.get(directory + File.separator + fileName);
        @NotNull final ObjectMapper mapper = new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @NotNull final Domain domain = mapper.readValue(path.toFile(), Domain.class);
        sessionService.remove(userSessionId);
        load(domain);
    }

    @Nullable
    public ProjectDTO getProjectDTO(@Nullable final Project project) {
        if (project == null) return null;
        @NotNull final ProjectDTO dto = new ProjectDTO();
        dto.setId(project.getId());
        dto.setUserId(project.getUser().getId());
        dto.setName(project.getName());
        dto.setDateCreate(project.getDateCreate());
        dto.setDateStart(project.getDateStart());
        dto.setDateFinish(project.getDateFinish());
        dto.setStatus(project.getStatus());
        dto.setInfo(project.getInfo());
        return dto;
    }

    @Nullable
    public TaskDTO getTaskDTO(@Nullable final Task task) {
        if (task == null) return null;
        @NotNull final TaskDTO dto = new TaskDTO();
        dto.setId(task.getId());
        dto.setUserId(task.getUser().getId());
        if (task.getProject() == null) dto.setProjectId(null);
        else dto.setProjectId(task.getProject().getId());
        dto.setName(task.getName());
        dto.setDateCreate(task.getDateCreate());
        dto.setDateStart(task.getDateStart());
        dto.setDateFinish(task.getDateFinish());
        dto.setStatus(task.getStatus());
        dto.setInfo(task.getInfo());
        return dto;
    }

    @Nullable
    public UserDTO getUserDTO(@Nullable final User user) {
        if (user == null) return null;
        @NotNull final UserDTO dto = new UserDTO();
        dto.setId(user.getId());
        dto.setName(user.getName());
        dto.setLogin(user.getLogin());
        dto.setPassword(user.getPassword());
        dto.setDateCreate(user.getDateCreate());
        dto.setDateStart(user.getDateStart());
        dto.setDateFinish(user.getDateFinish());
        dto.setRoleType(user.getRoleType());
        return dto;
    }

    @Nullable
    public SessionDTO getSessionDTO(@Nullable final Session session) {
        if (session == null) return null;
        @NotNull final SessionDTO dto = new SessionDTO();
        dto.setId(session.getId());
        dto.setUserId(session.getUser().getId());
        dto.setTimestamp(session.getTimestamp());
        dto.setRoleType(session.getRoleType());
        dto.setSignature(session.getSignature());
        return dto;
    }

    @Nullable
    public Project getProjectFromDTO(@Nullable final ProjectDTO dto) throws Exception {
        if (dto == null) return null;
        @NotNull final Project project = new Project();
        project.setId(dto.getId());
        @Nullable final User user = userService.findOne(dto.getUserId());
        if (user == null) throw new Exception();
        project.setUser(user);
        project.setName(dto.getName());
        project.setDateCreate(dto.getDateCreate());
        project.setDateStart(dto.getDateStart());
        project.setDateFinish(dto.getDateFinish());
        project.setStatus(dto.getStatus());
        project.setInfo(dto.getInfo());
        return project;
    }

    @Nullable
    public Task getTaskFromDTO(@Nullable final TaskDTO dto) throws Exception {
        if (dto == null) return null;
        @NotNull final Task task = new Task();
        task.setId(dto.getId());
        @Nullable final User user = userService.findOne(dto.getUserId());
        if (user == null) throw new Exception();
        task.setUser(user);
        if (dto.getProjectId() == null || dto.getProjectId().isEmpty()) task.setProject(null);
        else {
            @Nullable final Project project = projectService.findOne(dto.getProjectId());
            task.setProject(project);
        }
        task.setName(dto.getName());
        task.setDateCreate(dto.getDateCreate());
        task.setDateStart(dto.getDateStart());
        task.setDateFinish(dto.getDateFinish());
        task.setStatus(dto.getStatus());
        task.setInfo(dto.getInfo());
        return task;
    }

    @Nullable
    public User getUserFromDTO(@Nullable final UserDTO dto) {
        if (dto == null) return null;
        @NotNull final User user = new User();
        user.setId(dto.getId());
        user.setName(dto.getName());
        user.setLogin(dto.getLogin());
        user.setPassword(dto.getPassword());
        user.setDateCreate(dto.getDateCreate());
        user.setDateStart(dto.getDateStart());
        user.setDateFinish(dto.getDateFinish());
        user.setRoleType(dto.getRoleType());
        return user;
    }

    @Nullable
    public Session getSessionFromDTO(@Nullable final SessionDTO dto) throws Exception {
        if (dto == null) return null;
        @NotNull final Session session = new Session();
        session.setId(dto.getId());
        @Nullable final User user = userService.findOne(dto.getUserId());
        if (user == null) throw new Exception();
        session.setUser(user);
        session.setTimestamp(dto.getTimestamp());
        session.setRoleType(dto.getRoleType());
        session.setSignature(dto.getSignature());
        return session;
    }

    @Override
    public boolean isDirectory(@Nullable final String directory) throws Exception {
        if (directory == null || directory.isEmpty()) throw new Exception();
        return Files.isDirectory(Paths.get(directory));
    }

    @Override
    public boolean exist(@Nullable final String directory,
                         @Nullable final String fileName
    ) throws Exception {
        if  (directory == null || directory.isEmpty()) throw new Exception();
        if (fileName == null || fileName.isEmpty()) throw new Exception();
        return Files.exists(Paths.get(directory + File.separator + fileName));
    }

    @NotNull
    private Domain getInstance() throws Exception {
        @NotNull final Domain domain = new Domain();
        @Nullable final List<Project> projects = projectService.findAll();
        @Nullable final List<Task> tasks = taskService.findAll();
        @Nullable final List<User> users = userService.findAll();
        if (projects == null) throw new Exception();
        if (tasks == null) throw new Exception();
        if (users == null) throw new Exception();
        domain.setProjects(projects.stream().map(this::getProjectDTO).collect(Collectors.toList()));
        domain.setTasks(tasks.stream().map(this::getTaskDTO).collect(Collectors.toList()));
        domain.setUsers(users.stream().map(this::getUserDTO).collect(Collectors.toList()));
        return domain;
    }

    private void load(@Nullable final Domain domain) throws Exception {
        if (domain == null) throw new Exception();
        @Nullable final List<ProjectDTO> projects = domain.getProjects();
        @Nullable final List<TaskDTO> tasks = domain.getTasks();
        @Nullable final List<UserDTO> users = domain.getUsers();
        if (projects == null) throw new Exception();
        if (tasks == null) throw new Exception();
        if (users == null) throw new Exception();
        projectService.removeAll();
        taskService.removeAll();
        userService.removeAll();
        sessionService.removeAll();
        for (UserDTO dto : users) {
            @Nullable final User user = getUserFromDTO(dto);
            userService.persist(user);
        }
        for (ProjectDTO dto : projects) {
            @Nullable final Project project = getProjectFromDTO(dto);
            projectService.persist(project);
        }
        for (TaskDTO dto : tasks) {
            @Nullable final Task task = getTaskFromDTO(dto);
            taskService.persist(task);
        }
    }

}
