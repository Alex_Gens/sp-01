package ru.kazakov.iteco.context;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.kazakov.iteco.api.endpoint.*;
import javax.xml.ws.Endpoint;

@Component
@NoArgsConstructor
public class Bootstrap {

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private IDomainEndpoint domainEndpoint;

    @NotNull
    @Autowired
    private ISessionEndpoint sessionEndpoint;

    @NotNull
    @Value("${host}")
    private String host;

    @NotNull
    @Value("${port}")
    private String port;

    public void start() {
        Endpoint.publish("http://" + host + ":"
                                          + port
                                          + "/ProjectEndpoint?wsdl", projectEndpoint);
        Endpoint.publish("http://" + host + ":"
                                          + port
                                          + "/TaskEndpoint?wsdl", taskEndpoint);
        Endpoint.publish("http://" + host + ":"
                                          + port
                                          + "/UserEndpoint?wsdl", userEndpoint);
        Endpoint.publish("http://" + host + ":"
                                          + port
                                          + "/SessionEndpoint?wsdl", sessionEndpoint);
        Endpoint.publish("http://" + host + ":"
                                          + port
                                          + "/DomainEndpoint?wsdl", domainEndpoint);
    }

}
