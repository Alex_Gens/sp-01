package ru.kazakov.iteco.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.enumeration.RoleType;
import java.util.Date;

@Data
@NoArgsConstructor
public class UserDTO extends AbstractDTO {

    @Nullable
    private String name;

    @Nullable
    private String login;

    @Nullable
    private String password;

    @NotNull
    private Date dateCreate = new Date();

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @NotNull
    private RoleType roleType = RoleType.DEFAULT;



}
