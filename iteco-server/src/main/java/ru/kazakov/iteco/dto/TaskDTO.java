package ru.kazakov.iteco.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.enumeration.Status;
import java.util.Date;

@Data
@NoArgsConstructor
public class TaskDTO extends AbstractDTO {

    @NotNull
    private String userId;

    @Nullable
    private String projectId;

    @Nullable
    private String name;

    @NotNull
    private Date dateCreate = new Date();

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @NotNull
    private Status status = Status.PLANNED;

    @Nullable
    private String info;

}
