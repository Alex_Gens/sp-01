package ru.kazakov.iteco.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@XmlRootElement
@JsonAutoDetect
public class Domain implements Serializable {

    @Setter
    @Nullable
    private List<ProjectDTO> projects;

    @Setter
    @Nullable
    private List<TaskDTO> tasks;

    @Setter
    @Nullable
    private List<UserDTO> users;


    @Nullable
    @XmlElement(name = "project")
    @XmlElementWrapper(name = "projects")
    public List<ProjectDTO> getProjects() {return this.projects;}

    @Nullable
    @XmlElement(name = "task")
    @XmlElementWrapper(name = "tasks")
    public List<TaskDTO> getTasks() {return this.tasks;}

    @Nullable
    @XmlElement(name = "user")
    @XmlElementWrapper(name = "users")
    public List<UserDTO> getUsers() {return this.users;}

}
