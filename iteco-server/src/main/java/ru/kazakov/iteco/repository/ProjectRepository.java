package ru.kazakov.iteco.repository;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.kazakov.iteco.api.repository.IProjectRepository;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.enumeration.SortType;
import ru.kazakov.iteco.enumeration.Status;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;
import static ru.kazakov.iteco.constant.Constant.*;

@Repository
@Scope("prototype")
public class ProjectRepository implements IProjectRepository {

    @NotNull
    @Setter
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void persistProject(@NotNull final Project entity) {entityManager.persist(entity);}

    @Override
    public void mergeProject(@NotNull final Project entity) {entityManager.merge(entity);}

    @Override
    public void removeProject(@NotNull final String id) {
        @NotNull final String jpql = String.format("delete from Project p where p.%s = :%s", ID, ID);
        @NotNull final Query query = entityManager.createQuery(jpql);
        query.setParameter(ID, id);
        query.executeUpdate();
    }

    @Override
    public void removeAllProjects() {
        @NotNull final String jpql = "delete from Project p";
        @NotNull final Query query = entityManager.createQuery(jpql);
        query.executeUpdate();
    }

    @Override
    public void removeAllProjectsByCurrentId(@NotNull final String currentUserId) {
        @NotNull final String jpql = String.format("delete from Project p where p.%s.%s = :%s", USER, ID, USER_ID);
        @NotNull final Query query = entityManager.createQuery(jpql);
        query.setParameter(USER_ID, currentUserId);
        query.executeUpdate();
    }

    @Nullable
    @Override
    public Project findOneProject(@NotNull final String id) {
        @NotNull final String jpql = String.format("select p from Project p where p.%s = :%s", ID, ID);
        @NotNull final TypedQuery<Project> query = entityManager.createQuery(jpql, Project.class);
        query.setParameter(ID, id);
        @Nullable final Project project = query.getResultStream().findFirst().orElse(null);
        return project;
    }

    @Nullable
    @Override
    public Project findByProjectNameCurrentId(
            @NotNull final String currentUserId,
            @NotNull final String name) {
        @NotNull final String jpql = String.format("select p from Project p " +
                "where p.%s.%s = :%s and p.%s = :%s", USER, ID, USER_ID, NAME, NAME);
        @NotNull final TypedQuery<Project> query = entityManager.createQuery(jpql, Project.class);
        query.setParameter(USER_ID, currentUserId);
        query.setParameter(NAME, name);
        @Nullable final Project project = query.getResultStream().findFirst().orElse(null);
        return project;
    }

    @NotNull
    @Override
    public List<Project> findAllProjects() {
        @NotNull final String jpql = "select p from Project p";
        @NotNull final TypedQuery<Project> query = entityManager.createQuery(jpql, Project.class);
        @Nullable final List<Project> projects = query.getResultList();
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findAllProjectsByCurrentId(@NotNull final String currentUserId) {
        @NotNull final String jpql = String.format("select p from Project p " +
                "where p.%s.%s = :%s", USER, ID, USER_ID);
        @NotNull final TypedQuery<Project> query = entityManager.createQuery(jpql, Project.class);
        query.setParameter(USER_ID, currentUserId);
        @Nullable final List<Project> projects = query.getResultList();
        return projects;
    }

    @NotNull
    @Override
    public List<String> findAllSortedProjectsByCurrentId(
            @NotNull final String currentUserId,
            @NotNull final SortType sortType) {
        @NotNull final String orderBy;
        switch (sortType) {
            case START  : orderBy = String.format("p.%s, p.%s", DATE_START, DATE_CREATE); break;
            case FINISH : orderBy = String.format("p.%s, p.%s", DATE_FINISH, DATE_CREATE); break;
            case STATUS : orderBy = String.format("field(p.status, '%s','%s','%s'), p.%s",
                          String.valueOf(Status.PLANNED),
                          String.valueOf(Status.IN_PROGRESS),
                          String.valueOf(Status.READY),
                          DATE_CREATE); break;
            default     : orderBy = String.format("p.%s", DATE_CREATE);
        }
        @NotNull final String jpql = String.format("select p.%s from Project p where p.%s.%s = :%s order by %s",
                NAME, USER, ID, USER_ID, orderBy);
        @NotNull final TypedQuery<String> query = entityManager.createQuery(jpql, String.class);
        query.setParameter(USER_ID, currentUserId);
        @Nullable final List<String> names = query.getResultList();
        return names;
    }

    @NotNull
    @Override
    public List<String> findAllProjectsByNameCurrentId(
            @NotNull final String currentUserId,
            @NotNull final String part) {
        @NotNull final String jpql = String.format("select p.%s from Project p " +
                "where p.%s.%s = :%s and %s like :%s", NAME, USER, ID, USER_ID, NAME, NAME);
        @NotNull final TypedQuery<String> query = entityManager.createQuery(jpql, String.class);
        query.setParameter(USER_ID, currentUserId);
        query.setParameter(NAME, "%" + part + "%");
        @Nullable final List<String> names = query.getResultList();
        return names;
    }

    @NotNull
    @Override
    public List<String> findAllProjectsByInfoCurrentId(
            @NotNull final String currentUserId,
            @NotNull final String part) {
        @NotNull final String jpql = String.format("select p.%s from Project p " +
                "where p.%s.%s = :%s and p.info like :%s", NAME, USER, ID, USER_ID, INFORMATION, INFORMATION);
        @NotNull final TypedQuery<String> query = entityManager.createQuery(jpql, String.class);
        query.setParameter(USER_ID, currentUserId);
        query.setParameter(INFORMATION, "%" + part + "%");
        @Nullable final List<String> names = query.getResultList();
        return names;
    }

    @Override
    public boolean containsProjectByName(@NotNull final String name) {
        @NotNull final String jpql = String.format("select case when " +
                "(select p.%s from Project p where p.%s = :%s) is not null " +
                "then true else false end from Project p", ID, NAME, NAME);
        @NotNull final TypedQuery<Boolean> query = entityManager.createQuery(jpql, Boolean.class);
        query.setParameter(NAME, name);
        final boolean contains = query.getResultList().stream().findFirst().orElse(false);
        return contains;
    }

    @Override
    public boolean containsProjectByNameCurrentId(
            @NotNull final String currentUserId,
            @NotNull final String name) {
        @NotNull final String jpql = String.format("select case when " +
                "(select p.%s from Project p where p.%s.%s = :%s and p.%s = :%s) is not null " +
                "then true else false end from Project p", ID, USER, ID, USER_ID, NAME, NAME);
        @NotNull final TypedQuery<Boolean> query = entityManager.createQuery(jpql, Boolean.class);
        query.setParameter(USER_ID, currentUserId);
        query.setParameter(NAME, name);
        final boolean contains = query.getResultList().stream().findFirst().orElse(false);
        return contains;
    }

    @Override
    public boolean isEmptyProjectRepository(@NotNull final String currentUserId) {
        @NotNull final String jpql = String.format("select case when " +
                "(count(p.%s) > 0) " +
                "then false else true end from Project p where p.%s.%s = :%s", ID, USER, ID, USER_ID);
        @NotNull final TypedQuery<Boolean> query = entityManager.createQuery(jpql, Boolean.class);
        query.setParameter(USER_ID, currentUserId);
        final boolean isEmpty = query.getResultList().stream().findFirst().orElse(true);
        return isEmpty;
    }

}

