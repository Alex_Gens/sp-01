package ru.kazakov.iteco.repository;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.kazakov.iteco.api.repository.ITaskRepository;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.SortType;
import ru.kazakov.iteco.enumeration.Status;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;
import static ru.kazakov.iteco.constant.Constant.*;
import static ru.kazakov.iteco.constant.Constant.USER_ID;

@Repository
@Scope("prototype")
public class TaskRepository implements ITaskRepository {

    @NotNull
    @Setter
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void persistTask(@NotNull final Task entity) {entityManager.persist(entity);}

    @Override
    public void mergeTask(@NotNull final Task entity) {entityManager.merge(entity);}

    @Override
    public void removeTask(@NotNull final String id) {
        @NotNull final String jpql = String.format("delete from Task t where %s = :%s", ID, ID);
        @NotNull final Query query = entityManager.createQuery(jpql);
        query.setParameter(ID, id);
        query.executeUpdate();
    }

    @Override
    public void removeTasksWithProject(
            @NotNull final String currentUserId,
            @NotNull final String projectId) {
        @NotNull final String jpql = String.format("delete from Task t where" +
                " t.%s.%s = :%s and t.%s.%s = :%s", USER, ID, USER_ID, PROJECT, ID, PROJECT_ID);
        @NotNull final Query query = entityManager.createQuery(jpql);
        query.setParameter(USER_ID, currentUserId);
        query.setParameter(PROJECT_ID, projectId);
        query.executeUpdate();
    }

    @Override
    public void removeAllTasks() {
        @NotNull final String jpql = "delete from Task t";
        @NotNull final Query query = entityManager.createQuery(jpql);
        query.executeUpdate();
    }

    @Override
    public void removeAllTasksByCurrentId(
            @NotNull final String currentUserId) {
        @NotNull final String jpql = String.format("delete from Task t where t.%s.%s = :%s", USER, ID, USER_ID);
        @NotNull final Query query = entityManager.createQuery(jpql);
        query.setParameter(USER_ID, currentUserId);
        query.executeUpdate();
    }

    @Override
    public void removeAllTasksWithProjects(
            @NotNull final String currentUserId) {
        @NotNull final String jpql = String.format("delete from Task t " +
                "where t.%s.%s = :%s and t.%s is not null", USER, ID, USER_ID, PROJECT);
        @NotNull final Query query = entityManager.createQuery(jpql);
        query.setParameter(USER_ID, currentUserId);
        query.executeUpdate();
    }

    @Nullable
    @Override
    public Task findOneTask(@NotNull final String id) {
        @NotNull final String jpql = String.format("select t from Task t where t.%s = :%s", ID, ID);
        @NotNull final TypedQuery<Task> query = entityManager.createQuery(jpql, Task.class);
        query.setParameter(ID, id);
        @Nullable final Task task = query.getResultStream().findFirst().orElse(null);
        return task;
    }

    @Nullable
    @Override
    public Task findTaskByNameCurrentId(
            @NotNull final String currentUserId,
            @NotNull final String name) {
        @NotNull final String jpql = String.format("select t from Task t where " +
                "t.%s.%s = :%s and t.%s = :%s", USER, ID, USER_ID, NAME, NAME);
        @NotNull final TypedQuery<Task> query = entityManager.createQuery(jpql, Task.class);
        query.setParameter(USER_ID, currentUserId);
        query.setParameter(NAME, name);
        @Nullable final Task task = query.getResultStream().findFirst().orElse(null);
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAllTasks() {
        @NotNull final String jpql = "select t from Task t";
        @NotNull final TypedQuery<Task> query = entityManager.createQuery(jpql, Task.class);
        @Nullable final List<Task> tasks = query.getResultList();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findAllTasksByCurrentId(
            @NotNull final String currentUserId) {
        @NotNull final String jpql = String.format("select t from Task t " +
                "where t.%s.%s = :%s", USER, ID, USER_ID);
        @NotNull final TypedQuery<Task> query = entityManager.createQuery(jpql, Task.class);
        query.setParameter(USER_ID, currentUserId);
        @Nullable final List<Task> tasks = query.getResultList();
        return tasks;
    }

    @NotNull
    @Override
    public List<String> findAllSortedTasksByCurrentId(
            @NotNull final String currentUserId,
            @NotNull final SortType sortType) {
        @NotNull final String orderBy;
        switch (sortType) {
            case START  : orderBy = String.format("t.%s, t.%s", DATE_START, DATE_CREATE); break;
            case FINISH : orderBy = String.format("t.%s, t.%s", DATE_FINISH, DATE_CREATE); break;
            case STATUS : orderBy = String.format("field(t.status, '%s','%s','%s'), t.%s",
                          String.valueOf(Status.PLANNED),
                          String.valueOf(Status.IN_PROGRESS),
                          String.valueOf(Status.READY),
                          DATE_CREATE); break;
            default     : orderBy = String.format("t.%s", DATE_CREATE);
        }
        @NotNull final String jpql = String.format("select t.%s from Task t where t.%s.%s = :%s order by %s",
                NAME, USER, ID, USER_ID, orderBy);
        @NotNull final TypedQuery<String> query = entityManager.createQuery(jpql, String.class);
        query.setParameter(USER_ID, currentUserId);
        @Nullable final List<String> names = query.getResultList();
        return names;
    }

    @NotNull
    @Override
    public List<String> findAllSortedTasksByCurrentIdProjectId(
            @NotNull final String currentUserId,
            @NotNull final String projectId,
            @NotNull final SortType sortType) {
        @NotNull final String orderBy;
        switch (sortType) {
            case START  : orderBy = String.format("t.%s, t.%s", DATE_START, DATE_CREATE); break;
            case FINISH : orderBy = String.format("t.%s, t.%s", DATE_FINISH, DATE_CREATE); break;
            case STATUS : orderBy = String.format("field(t.status, '%s','%s','%s'), t.%s",
                          String.valueOf(Status.PLANNED),
                          String.valueOf(Status.IN_PROGRESS),
                          String.valueOf(Status.READY),
                          DATE_CREATE); break;
            default     : orderBy = String.format("t.%s", DATE_CREATE);
        }
        @NotNull final String jpql = String.format("select t.%s from Task t " +
                "where t.%s.%s = :%s and t.%s.%s = :%s order by %s",
                NAME, USER, ID, USER_ID, PROJECT, ID, PROJECT_ID, orderBy);
        @NotNull final TypedQuery<String> query = entityManager.createQuery(jpql, String.class);
        query.setParameter(USER_ID, currentUserId);
        query.setParameter(PROJECT_ID, projectId);
        @Nullable final List<String> names = query.getResultList();
        return names;
    }

    @NotNull
    @Override
    public List<String> findAllTasksByNameCurrentId(
            @NotNull final String currentUserId,
            @NotNull final String part) {
        @NotNull final String jpql = String.format("select t.%s from Task t " +
                "where t.%s.%s = :%s and t.%s like :%s", NAME, USER, ID, USER_ID, NAME, NAME);
        @NotNull final TypedQuery<String> query = entityManager.createQuery(jpql, String.class);
        query.setParameter(USER_ID, currentUserId);
        query.setParameter(NAME, "%" + part + "%");
        @Nullable final List<String> names = query.getResultList();
        return names;
    }

    @NotNull
    @Override
    public List<String> findAllTasksByInfoCurrentId(
            @NotNull final String currentUserId,
            @NotNull final String part) {
        @NotNull final String jpql = String.format("select t.%s from Task t " +
                "where t.%s.%s = :%s and t.info like :%s", NAME, USER, ID, USER_ID, INFORMATION);
        @NotNull final TypedQuery<String> query = entityManager.createQuery(jpql, String.class);
        query.setParameter(USER_ID, currentUserId);
        query.setParameter(INFORMATION, "%" + part + "%");
        @Nullable final List<String> names = query.getResultList();
        return names;
    }

    @Override
    public boolean containsTaskByName(
            @NotNull final String name) {
        @NotNull final String jpql = String.format("select case when " +
                "(select t.%s from Task t where t.%s = :%s) is not null " +
                "then true else false end from Task t", ID, NAME, NAME);
        @NotNull final TypedQuery<Boolean> query = entityManager.createQuery(jpql, Boolean.class);
        query.setParameter(NAME, name);
        final boolean contains = query.getResultList().stream().findFirst().orElse(false);
        return contains;
    }

    @Override
    public boolean containsTaskByNameCurrentId(
            @NotNull final String currentUserId,
            @NotNull final String name) {
        @NotNull final String jpql = String.format("select case when " +
                "(select t.%s from Task t where t.%s.%s = :%s and t.%s = :%s) is not null " +
                "then true else false end from Task t", ID, USER, ID, USER_ID, NAME, NAME);
        @NotNull final TypedQuery<Boolean> query = entityManager.createQuery(jpql, Boolean.class);
        query.setParameter(USER_ID, currentUserId);
        query.setParameter(NAME, name);
        final boolean contains = query.getResultList().stream().findFirst().orElse(false);
        return contains;
    }

    @Override
    public boolean isEmptyTaskRepository(
            @NotNull final String currentUserId) {
        @NotNull final String jpql = String.format("select case when " +
                "(count(t.%s) > 0) " +
                "then false else true end from Task t where t.%s.%s = :%s", ID, USER, ID, USER_ID);
        @NotNull final TypedQuery<Boolean> query = entityManager.createQuery(jpql, Boolean.class);
        query.setParameter(USER_ID, currentUserId);
        final boolean contains = query.getResultList().stream().findFirst().orElse(false);
        return contains;
    }

}
