package ru.kazakov.iteco.repository;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.kazakov.iteco.api.repository.ISessionRepository;
import ru.kazakov.iteco.entity.Session;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;
import static ru.kazakov.iteco.constant.Constant.*;

@Repository
@Scope("prototype")
public class SessionRepository implements ISessionRepository {

    @NotNull
    @Setter
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void persistSession(@NotNull final Session entity) {entityManager.persist(entity);}

    @Override
    public void removeSession(@NotNull final String id) {
        @Nullable final Session session = findOneSession(id);
        entityManager.remove(session);
    }

    @Override
    public void removeSessionByUserId(@NotNull final String userId) {
        @NotNull final String jpql = String.format("delete from Session s " +
                "where s.%s.%s = :%s", USER, ID, USER_ID);
        @NotNull final Query query = entityManager.createQuery(jpql);
        query.setParameter(USER_ID, userId);
        query.executeUpdate();
    }

    @Override
    public void removeAllSessions() {
        @NotNull final String jpql = "delete from Session s";
        @NotNull final Query query = entityManager.createQuery(jpql);
        query.executeUpdate();
    }

    @Nullable
    @Override
    public Session findOneSession(@NotNull final String id) {
        @NotNull final String jpql = String.format("select s from Session s where s.%s = :%s", ID, ID);
        @NotNull final TypedQuery<Session> query = entityManager.createQuery(jpql, Session.class);
        query.setParameter(ID, id);
        @Nullable final Session session = query.getSingleResult();
        return session;
    }

    @NotNull
    @Override
    public List<Session> findAllSessions() {
        @NotNull final String jpql = "select s from Session s";
        @NotNull final TypedQuery<Session> query = entityManager.createQuery(jpql, Session.class);
        @Nullable final List<Session> sessions =  query.getResultList();
        return sessions;
    }

    @Override
    public boolean containsSession(@NotNull final String userId,
                                   @NotNull final String id) {
        @NotNull final String jpql = String.format("select case when " +
                "(select s.%s from Session s where s.%s = :%s and s.%s.%s = :%s) is not null " +
                "then true else false end from Session s", ID, ID, ID, USER, ID, USER_ID);
        @NotNull final TypedQuery<Boolean> query = entityManager.createQuery(jpql, Boolean.class);
        query.setParameter(ID, id);
        query.setParameter(USER_ID, userId);
        final boolean contains = query.getSingleResult();
        return contains;
    }

}
