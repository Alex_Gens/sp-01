package ru.kazakov.iteco;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.kazakov.iteco.configuration.ApplicationConfig;
import ru.kazakov.iteco.context.Bootstrap;

public class Application {

    public static void main(String[] args) {
        ApplicationContext context =
                new AnnotationConfigApplicationContext(ApplicationConfig.class);
        @NotNull final Bootstrap bootstrap = context.getBean("bootstrap", Bootstrap.class);
        bootstrap.start();
    }

}
