package ru.kazakov.iteco.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.dto.ProjectDTO;
import ru.kazakov.iteco.enumeration.SortType;
import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @WebMethod
    public void mergeProject(
            @Nullable final String token,
            @Nullable final ProjectDTO dto
    ) throws Exception;

    @WebMethod
    public void persistProject(
            @Nullable final String token,
            @Nullable final ProjectDTO dto
    ) throws Exception;

    @WebMethod
    public void createProject(
            @Nullable final String token,
            @Nullable final String name
    ) throws Exception;

    @WebMethod
    public void removeProjectById(
            @Nullable final String token,
            @Nullable final String id
    ) throws Exception;

    @WebMethod
    public void removeAllProjects(@Nullable final String token) throws Exception;

    @WebMethod
    public void removeAllProjectsByCurrentId(@Nullable final String token) throws Exception;

    @WebMethod
    public @Nullable ProjectDTO findByProjectNameCurrentId(
            @Nullable final String token,
            @Nullable final String name
    ) throws Exception;

    @Nullable
    @WebMethod
    public ProjectDTO findOneProject(
            @Nullable final String token,
            @Nullable final String id
    ) throws Exception;

    @NotNull
    @WebMethod
    public List<ProjectDTO> findAllProjects(@Nullable final String token) throws Exception;

    @NotNull
    @WebMethod
    public List<ProjectDTO> findAllProjectsByCurrentId(@Nullable final String token) throws Exception;

    @Nullable
    @WebMethod
    public List<String> findAllSortedProjectsByCurrentId(
            @Nullable final String token,
            @Nullable final SortType sortType
    ) throws Exception;

    @Nullable
    @WebMethod
    public List<String> findAllProjectsByNameCurrentId(
            @Nullable final String token,
            @Nullable final String part
    ) throws Exception;

    @Nullable
    @WebMethod
    public List<String> findAllProjectsByInfoCurrentId(
            @Nullable final String token,
            @Nullable final String part
    ) throws Exception;

    @WebMethod
    public boolean containsProject(
            @Nullable final String name,
            @Nullable final String token
    ) throws Exception;

    @WebMethod
    public boolean containsProjectByCurrentId(
            @Nullable final String token,
            @Nullable final String name
    ) throws Exception;

    @WebMethod
    public boolean isEmptyProjectRepositoryByCurrentId(@Nullable final String token) throws Exception;

}
