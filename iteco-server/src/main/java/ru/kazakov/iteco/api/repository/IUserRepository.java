package ru.kazakov.iteco.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.User;
import java.sql.SQLException;
import java.util.List;

public interface IUserRepository {

    public void persistUser(@NotNull final User entity);

    public void mergeUser(@NotNull final User entity);

    public void removeUser(@NotNull final String id);

    public void removeAllUsers();

    @Nullable
    public User findOneUser(@NotNull final String id);

    @Nullable
    public User findUserByLogin(@NotNull final String login);

    @NotNull
    public List<User> findAllUsers();

    @NotNull
    public List<String> findAllUsersLogins() throws SQLException;

    public boolean containsUserByLogin(@NotNull final String login);

}
