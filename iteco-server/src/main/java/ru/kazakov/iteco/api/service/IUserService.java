package ru.kazakov.iteco.api.service;

import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.User;
import java.util.List;

public interface IUserService extends IService<User> {

    public void merge(@Nullable final User entity) throws Exception;

    @Nullable
    public User findByLogin(@Nullable final String login) throws Exception;

    @Nullable
    public User findCurrentUser(@Nullable final String token) throws Exception;

    @Nullable
    public List<String> findAllUsersLogins() throws Exception;

    public boolean contains(@Nullable final String login) throws Exception;

}
