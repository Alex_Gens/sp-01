package ru.kazakov.iteco.api.service;

import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.dto.SessionDTO;
import ru.kazakov.iteco.entity.Session;
import ru.kazakov.iteco.enumeration.RoleType;

public interface ISessionService extends IService<Session> {

    public RoleType getRoleType(@Nullable final String token) throws Exception;

    @Nullable
    public String getInstance(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Session userSession,
            @Nullable final SessionDTO dto
    ) throws Exception;

    public void removeByUserId(@Nullable final String userId) throws Exception;

    public boolean contains(
            @Nullable final String userId,
            @Nullable final String id
    ) throws Exception;

}
