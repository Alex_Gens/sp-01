package ru.kazakov.iteco.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.SortType;
import java.util.List;

public interface ITaskRepository {

    public void persistTask(@NotNull final Task entity);

    public void mergeTask(@NotNull final Task entity);

    public void removeTask(@NotNull final String id);

    public void removeTasksWithProject(
            @NotNull final String currentUserId,
            @NotNull final String projectId);

    public void removeAllTasks();

    public void removeAllTasksByCurrentId(@NotNull final String currentUserId);

    public void removeAllTasksWithProjects(@NotNull final String currentUserId);

    @Nullable
    public Task findOneTask(@NotNull final String id);

    @Nullable
    public Task findTaskByNameCurrentId(
            @NotNull final String currentUserId,
            @NotNull final String name);

    @NotNull
    public List<Task> findAllTasks();

    @NotNull
    public List<Task> findAllTasksByCurrentId(@NotNull final String currentUserId);

    @NotNull
    public List<String> findAllSortedTasksByCurrentId(
            @NotNull final String currentUserId,
            @NotNull final SortType sortType);

    @NotNull
    public List<String> findAllSortedTasksByCurrentIdProjectId(
            @NotNull final String currentUserId,
            @NotNull final String projectId,
            @NotNull final SortType sortType);

    @NotNull
    public List<String> findAllTasksByNameCurrentId(
            @NotNull final String currentUserId,
            @NotNull final String part);

    @NotNull
    public List<String> findAllTasksByInfoCurrentId(
            @NotNull final String currentUserId,
            @NotNull final String part);

    public boolean containsTaskByName(@NotNull final String name);

    public boolean containsTaskByNameCurrentId(
            @NotNull final String currentUserId,
            @NotNull final String name);

    public boolean isEmptyTaskRepository(@NotNull final String currentUserId);

}
