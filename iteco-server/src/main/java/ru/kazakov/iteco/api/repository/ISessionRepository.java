package ru.kazakov.iteco.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Session;
import java.util.List;

public interface ISessionRepository {

    public void persistSession(@NotNull final Session entity);

    public void removeSession(@NotNull final String id);

    public void removeSessionByUserId(String userId);

    public void removeAllSessions();

    @Nullable
    public Session findOneSession(@NotNull final String id);

    @NotNull
    public List<Session> findAllSessions();

    public boolean containsSession(
            @NotNull final String userId,
            @NotNull final String id);

}
