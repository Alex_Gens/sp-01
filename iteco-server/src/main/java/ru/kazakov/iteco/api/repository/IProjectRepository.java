package ru.kazakov.iteco.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.enumeration.SortType;
import java.util.List;

public interface IProjectRepository {

    public void persistProject(@NotNull final Project entity);

    public void mergeProject(@NotNull final Project entity);

    public void removeProject(@NotNull final String id);

    public void removeAllProjects();

    public void removeAllProjectsByCurrentId(@NotNull final String currentUserId);

    @Nullable
    public Project findOneProject(@NotNull final String id);

    @Nullable
    public Project findByProjectNameCurrentId(
            @NotNull final String currentUserId,
            @NotNull final String name);

    @NotNull
    public List<Project> findAllProjects();

    @NotNull
    public List<Project> findAllProjectsByCurrentId(@NotNull final String currentUserId);

    @NotNull
    public List<String> findAllSortedProjectsByCurrentId(
            @NotNull final String currentUserId,
            @NotNull final SortType sortType);

    @NotNull
    public List<String> findAllProjectsByNameCurrentId(
            @NotNull final String currentUserId,
            @NotNull final String part);

    @NotNull
    public List<String> findAllProjectsByInfoCurrentId(
            @NotNull final String currentUserId,
            @NotNull final String part);

    public boolean containsProjectByName(@NotNull final String name);

    public boolean containsProjectByNameCurrentId(
            @NotNull final String currentUserId,
            @NotNull final String name);

    public boolean isEmptyProjectRepository(@NotNull final String currentUserId);

}
