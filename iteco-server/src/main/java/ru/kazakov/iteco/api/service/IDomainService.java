package ru.kazakov.iteco.api.service;

import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.dto.ProjectDTO;
import ru.kazakov.iteco.dto.SessionDTO;
import ru.kazakov.iteco.dto.TaskDTO;
import ru.kazakov.iteco.dto.UserDTO;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.entity.Session;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.entity.User;

public interface IDomainService {

    public void saveBin(
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    public void saveJaxbXml(
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    public void saveJaxbJson(
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    public void saveFasterXml(
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    public void saveFasterJson(
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    public void loadBin(
            @Nullable final String userSessionId,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    public void loadJaxbXml(
            @Nullable final String userSessionId,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    public void loadJaxbJson(
            @Nullable final String userSessionId,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    public void loadFasterXml(
            @Nullable final String userSessionId,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    public void loadFasterJson(
            @Nullable final String userSessionId,
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    public boolean isDirectory(@Nullable final String directory) throws Exception;

    public boolean exist(
            @Nullable final String directory,
            @Nullable final String fileName
    ) throws Exception;

    @Nullable
    public ProjectDTO getProjectDTO(@Nullable final Project project);

    @Nullable
    public TaskDTO getTaskDTO(@Nullable final Task task);

    @Nullable
    public UserDTO getUserDTO(@Nullable final User user);

    @Nullable
    public SessionDTO getSessionDTO(@Nullable final Session Session);

    @Nullable
    public Project getProjectFromDTO(@Nullable final ProjectDTO dto) throws Exception;

    @Nullable
    public Task getTaskFromDTO(@Nullable final TaskDTO dto) throws Exception;

    @Nullable
    public User getUserFromDTO(@Nullable final UserDTO dto);

    @Nullable
    public Session getSessionFromDTO(@Nullable final SessionDTO dto) throws Exception;

}
