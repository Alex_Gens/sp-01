package ru.kazakov.iteco.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.dto.UserDTO;
import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {

    @WebMethod
    void mergeUser(
            @Nullable final String token,
            @Nullable UserDTO dto
    ) throws Exception;

    @WebMethod
    void persistUser(@Nullable final UserDTO dto) throws Exception;

    @WebMethod
    public void removeUser(
            @Nullable final String token,
            @Nullable final String id
    ) throws Exception;

    @WebMethod
    public void removeAllUsers(@Nullable final String token) throws Exception;

    @Nullable
    @WebMethod
    public UserDTO findOneUser(
            @Nullable final String token,
            @Nullable final String id
    ) throws Exception;

    @Nullable
    @WebMethod
    public UserDTO findUserByLogin(
            @Nullable final String token,
            @Nullable final String login
    ) throws Exception;

    @Nullable
    @WebMethod
    public UserDTO findCurrentUser(@Nullable final String token) throws Exception;

    @NotNull
    @WebMethod
    public List<UserDTO> findAllUsers(@Nullable final String token) throws Exception;

    @Nullable
    @WebMethod
    public List<String> findAllUsersLogins(@Nullable final String token) throws Exception;

    @WebMethod
    public boolean containsUser(@Nullable final String login) throws Exception;

}
