package ru.kazakov.iteco.endpoint;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.kazakov.iteco.api.endpoint.ITaskEndpoint;
import ru.kazakov.iteco.api.service.IDomainService;
import ru.kazakov.iteco.api.service.ISessionService;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.dto.SessionDTO;
import ru.kazakov.iteco.dto.TaskDTO;
import ru.kazakov.iteco.entity.Session;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.SortType;
import ru.kazakov.iteco.util.AES;
import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@Component
@WebService(endpointInterface = "ru.kazakov.iteco.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    @Value("${secret}")
    private String secret;

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IDomainService domainService;

    @Autowired
    public TaskEndpoint(@NotNull final ISessionService sessionService, 
                        @NotNull final ITaskService taskService, 
                        @NotNull final IDomainService domainService) {
        super(sessionService);
        this.taskService = taskService;
        this.domainService = domainService;
    }

    @Override
    @WebMethod
    public void mergeTask(
            @Nullable final String token,
            @Nullable final TaskDTO dto
    ) throws Exception {
        validate(token);
        @Nullable final Task entity = domainService.getTaskFromDTO(dto);
        taskService.merge(entity);
    }

    @Override
    @WebMethod
    public void persistTask(
            @Nullable final String token,
            @Nullable final TaskDTO dto
    ) throws Exception {
        validate(token);
        @Nullable final Task entity = domainService.getTaskFromDTO(dto);
        taskService.persist(entity);
    }

    @Override
    @WebMethod
    public void createTask(
            @Nullable final String token,
            @Nullable final String name
    ) throws Exception {
        validate(token);
        @NotNull final String decryptToken = AES.decrypt(token, secret);
        @NotNull final ObjectMapper mapper = new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @Nullable final SessionDTO dto = mapper.readValue(decryptToken, SessionDTO.class);
        @Nullable final Session session = domainService.getSessionFromDTO(dto);
        taskService.create(session, name);
    }

    @Override
    @WebMethod
    public void removeTaskById(
            @Nullable final String token,
            @Nullable final String id
    ) throws Exception {
        validate(token);
        taskService.remove(id);
    }

    @Override
    @WebMethod
    public void removeTasksWithProject(
            @Nullable final String token,
            @Nullable final String projectId
    ) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        taskService.removeWithProject(currentId, projectId);
    }

    @Override
    @WebMethod
    public void removeAllTasks(@Nullable final String token) throws Exception {
        validate(token);
        taskService.removeAll();
    }

    @Override
    @WebMethod
    public void removeAllTasksByCurrentId(@Nullable final String token) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        taskService.removeAll(currentId);
    }

    @Override
    @WebMethod
    public void removeAllTasksWithProjects(@Nullable final String token) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        taskService.removeAllWithProjects(currentId);
    }

    @Nullable
    @Override
    @WebMethod
    public TaskDTO findByTaskNameCurrentId(
            @Nullable final String token,
            @Nullable final String name
    ) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        @Nullable final Task entity = taskService.findByName(currentId, name);
        return domainService.getTaskDTO(entity);
    }

    @Nullable
    @Override
    @WebMethod
    public TaskDTO findOneTask(
            @Nullable final String token,
            @Nullable final String id
    ) throws Exception {
        validate(token);
        @Nullable final Task entity = taskService.findOne(id);
        return domainService.getTaskDTO(entity);
    }

    @Nullable
    @Override
    @WebMethod
    public List<TaskDTO> findAllTasks(@Nullable final String token) throws Exception {
        validate(token);
        List<Task> list = taskService.findAll();
        return list.stream().map(domainService::getTaskDTO).collect(Collectors.toList());
    }

    @Nullable
    @Override
    @WebMethod
    public List<String> findAllSortedTasksByCurrentIdProjectId(
            @Nullable final String token,
            @Nullable final String projectId,
            @Nullable final SortType sortType
    ) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        return taskService.findAll(currentId, projectId, sortType);
    }

    @Nullable
    @Override
    @WebMethod
    public List<TaskDTO> findAllTasksByCurrentId(@Nullable final String token) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        List<Task> list = taskService.findAll(currentId);
        return list.stream().map(domainService::getTaskDTO).collect(Collectors.toList());
    }

    @Nullable
    @Override
    @WebMethod
    public List<String> findAllSortedTasksByCurrentId(
            @Nullable final String token,
            @Nullable final SortType sortType
    ) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        return taskService.findAll(currentId, sortType);
    }

    @Nullable
    @Override
    @WebMethod
    public List<String> findAllTasksByNameCurrentId(
            @Nullable final String token,
            @Nullable final String part
    ) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        return taskService.findAllByName(currentId, part);
    }

    @Nullable
    @Override
    @WebMethod
    public List<String> findAllTasksByInfoCurrentId(
            @Nullable final String token,
            @Nullable final String part
    ) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        return taskService.findAllByInfo(currentId, part);
    }

    @Override
    @WebMethod
    public boolean containsTask(
            @Nullable final String token,
            @Nullable final String name
    ) throws Exception {
        validate(token);
        return taskService.contains(name);
    }

    @Override
    @WebMethod
    public boolean containsTaskByCurrentId(
            @Nullable final String token,
            @Nullable final String name
    ) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        return taskService.contains(currentId, name);
    }

    @Override
    @WebMethod
    public boolean isEmptyTaskRepositoryByCurrentId(@Nullable final String token) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        return taskService.isEmptyRepository(currentId);
    }

}
