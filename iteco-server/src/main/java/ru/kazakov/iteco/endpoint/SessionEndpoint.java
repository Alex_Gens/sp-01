package ru.kazakov.iteco.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kazakov.iteco.api.endpoint.ISessionEndpoint;
import ru.kazakov.iteco.api.service.IDomainService;
import ru.kazakov.iteco.api.service.ISessionService;
import ru.kazakov.iteco.dto.SessionDTO;
import ru.kazakov.iteco.entity.Session;
import ru.kazakov.iteco.enumeration.RoleType;
import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@Component
@WebService(endpointInterface = "ru.kazakov.iteco.api.endpoint.ISessionEndpoint")
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @NotNull
    private final IDomainService domainService;

    @Autowired
    public SessionEndpoint(@NotNull final ISessionService sessionService,
                           @NotNull final IDomainService domainService) {
        super(sessionService);
        this.domainService = domainService;
    }

    @NotNull
    @Override
    @WebMethod
    public RoleType getSessionRoleType(@Nullable final String token) throws Exception {
        return sessionService.getRoleType(token);
    }

    @Nullable
    @Override
    @WebMethod
    public String getInstanceToken(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception {
        @NotNull final Session userSession = new Session();
        @Nullable final SessionDTO dto = new SessionDTO();
        dto.setId(userSession.getId());
        return sessionService.getInstance(login, password, userSession, dto);
    }

    @Override
    @WebMethod
    public void persistSession(@Nullable final SessionDTO dto) throws Exception {
        @Nullable final Session entity = domainService.getSessionFromDTO(dto);
        sessionService.persist(entity);}

    @Override
    @WebMethod
    public void removeSession(@Nullable final String id) throws Exception {
        sessionService.remove(id);
    }

    @Override
    @WebMethod
    public void removeSessionByUserId(@Nullable final String userId) throws Exception {
        sessionService.removeByUserId(userId);
    }

    @Override
    @WebMethod
    public void removeAllSessions() throws Exception {sessionService.removeAll();}

    @Nullable
    @Override
    @WebMethod
    public SessionDTO findOneSession(@Nullable final String id) throws Exception {
        @Nullable final Session entity = sessionService.findOne(id);
        return domainService.getSessionDTO(entity);
    }

    @NotNull
    @Override
    @WebMethod
    public  List<SessionDTO> findAllSessions() throws Exception {
        List<Session> list = sessionService.findAll();
        return list.stream().map(v -> domainService.getSessionDTO(v)).collect(Collectors.toList());
    }

    @Override
    @WebMethod
    public boolean containsSession(
            @Nullable final String userId,
            @Nullable final String id
    ) throws Exception {
        return sessionService.contains(userId, id);
    }

}
