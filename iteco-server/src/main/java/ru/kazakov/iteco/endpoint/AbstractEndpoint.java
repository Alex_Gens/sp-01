package ru.kazakov.iteco.endpoint;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.kazakov.iteco.api.service.ISessionService;
import ru.kazakov.iteco.constant.Constant;
import ru.kazakov.iteco.dto.SessionDTO;
import ru.kazakov.iteco.entity.Session;
import ru.kazakov.iteco.util.AES;
import ru.kazakov.iteco.util.SignatureUtil;

@Component
public abstract class AbstractEndpoint {

    @NotNull
    protected final ISessionService sessionService;

    @NotNull
    @Value("${secret}")
    protected String secret;

    @Autowired
    public AbstractEndpoint(@NotNull final ISessionService sessionService) {
        this.sessionService = sessionService;
    }

    protected void validate(@Nullable final String token) throws Exception {
        if(token == null || token.isEmpty()) throw new Exception();
        @NotNull final String decryptToken = AES.decrypt(token, secret);
        @NotNull final ObjectMapper mapper = new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @Nullable final SessionDTO dto = mapper.readValue(decryptToken, SessionDTO.class);
        if (dto == null) throw new Exception();
        if (!sessionService.contains(dto.getUserId(), dto.getId())) throw new Exception();
        @Nullable final Session session = sessionService.findOne(dto.getId());
        if (session == null) throw new Exception();
        if (session.getSignature() == null) throw new Exception();
        dto.setSignature(null);
        if (!session.getSignature().equals(
                SignatureUtil.getSignature(dto))) throw new Exception("Session is invalid");
        if (System.currentTimeMillis() - dto.getTimestamp()
                > Constant.SESSION_TIME) throw new Exception("Session time out\n you need to log in again");
    }

    protected String validateReturnCurrentId(@Nullable final String token) throws Exception {
        if(token == null || token.isEmpty()) throw new Exception();
        @NotNull final String decryptToken = AES.decrypt(token, secret);
        @NotNull final ObjectMapper mapper = new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @Nullable final SessionDTO dto = mapper.readValue(decryptToken, SessionDTO.class);
        if (dto == null) throw new Exception();
        if (!sessionService.contains(dto.getUserId(), dto.getId())) throw new Exception();
        @Nullable final Session session = sessionService.findOne(dto.getId());
        if (session == null) throw new Exception();
        if (session.getSignature() == null) throw new Exception();
        dto.setSignature(null);
        if (!session.getSignature().equals(
                SignatureUtil.getSignature(dto))) throw new Exception("Session is invalid");
        if (System.currentTimeMillis() - dto.getTimestamp()
                > Constant.SESSION_TIME) throw new Exception("Session time out\n you need to log in again");
        return dto.getUserId();
    }

}
