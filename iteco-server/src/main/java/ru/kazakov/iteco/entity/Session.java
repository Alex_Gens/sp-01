package ru.kazakov.iteco.entity;

import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.enumeration.RoleType;
import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "sessions")
public class Session extends AbstractEntity {

    @NotNull
    @ManyToOne
    private User user;

    @Column
    private long timestamp = System.currentTimeMillis();

    @Column(name = "role_type")
    @NotNull
    @Enumerated(EnumType.STRING)
    private RoleType roleType;

    @Column
    @Nullable
    private  String signature;

}
