package ru.kazakov.iteco.entity;

import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.enumeration.Status;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "projects")
public class Project extends AbstractEntity {

    @NotNull
    @ManyToOne
    private User user;

    @Column
    @Nullable
    private String name;

    @Column(name = "date_create")
    @NotNull
    private Date dateCreate = new Date();

    @Column(name = "date_start")
    @Nullable
    private Date dateStart;

    @Column(name = "date_finish")
    @Nullable
    private Date dateFinish;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

    @Column(name = "information")
    @Nullable
    private String info;

    @Nullable
    @OneToMany(mappedBy = "project")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Collection<Task> tasks = new ArrayList<>();

    public boolean isEmpty() {return info == null || info.isEmpty();}

}
