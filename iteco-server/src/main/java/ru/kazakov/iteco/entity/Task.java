package ru.kazakov.iteco.entity;

import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.enumeration.Status;
import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tasks")
public class Task extends AbstractEntity {

    @NotNull
    @ManyToOne
    private User user;

    @Nullable
    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    @Column
    @Nullable
    private String name;

    @Column(name = "date_create")
    @NotNull
    private Date dateCreate = new Date();

    @Column(name = "date_start")
    @Nullable
    private Date dateStart;

    @Column(name = "date_finish")
    @Nullable
    private Date dateFinish;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

    @Column(name = "information")
    @Nullable
    private String info;

    public boolean isEmpty() {return info == null || info.isEmpty();}



}
