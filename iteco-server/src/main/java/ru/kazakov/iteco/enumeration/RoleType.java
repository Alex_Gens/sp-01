package ru.kazakov.iteco.enumeration;

import org.jetbrains.annotations.NotNull;

public enum RoleType {

    NONE("None"),
    DEFAULT("Default"),
    ADMINISTRATOR("Administrator");

    @NotNull
    private String displayName;

    RoleType(@NotNull final String displayName) {this.displayName = displayName;}

    @NotNull
    public String getDisplayName() {return this.displayName;}

}
