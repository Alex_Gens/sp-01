package ru.kazakov.iteco.constant;

import org.jetbrains.annotations.NotNull;

public class Constant {

    @NotNull
    public static final String SALT = "@JF27$o%";

    public static final int CYCLE = 60000;

    public static final long SESSION_TIME = 1800000;

    @NotNull
    public static final String ID = "id";

    @NotNull
    public static final String USER_ID = "userId";

    @NotNull
    public static final String NAME = "name";

    @NotNull
    public static final String DATE_CREATE = "dateCreate";

    @NotNull
    public static final String DATE_START = "dateStart";

    @NotNull
    public static final String DATE_FINISH = "dateFinish";

    @NotNull
    public static final String STATUS = "status";

    @NotNull
    public static final String INFORMATION = "information";

    @NotNull
    public static final String PROJECT_ID = "projectId";

    @NotNull
    public static final String LOGIN = "login";

    @NotNull
    public static final String PASSWORD = "password";

    @NotNull
    public static final String ROLE_TYPE = "roleType";

    @NotNull
    public static final String TIMESTAMP = "timestamp";

    @NotNull
    public static final String SIGNATURE = "signature";

    @NotNull
    public static final String USER = "user";

    @NotNull
    public static final String PROJECT = "project";

}
