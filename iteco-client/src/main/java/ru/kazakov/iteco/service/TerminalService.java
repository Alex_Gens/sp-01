package ru.kazakov.iteco.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.kazakov.iteco.api.service.ITerminalService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Component
public final class TerminalService implements ITerminalService {

    @NotNull
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    @NotNull
    private static final String lineSeparator = System.lineSeparator();

    @NotNull
    @Override
    public String enterIgnoreEmpty() throws IOException {
        @NotNull String name = reader.readLine();
        while (name.isEmpty()) name = reader.readLine();
        return name;
    }

    @NotNull
    @Override
    public String read() throws IOException {
        @NotNull final StringBuilder builder = new StringBuilder().append("");
        @NotNull String temp = reader.readLine();
        while (!temp.equals("-save")) {
            builder.append(temp);
            builder.append(lineSeparator);
            temp = reader.readLine();
        }
        return builder.toString();
    }

    @Override
    public void write(@NotNull final String text) {System.out.println(text);}

    @Override
    public void separateLines() {System.out.print(lineSeparator);}

}
