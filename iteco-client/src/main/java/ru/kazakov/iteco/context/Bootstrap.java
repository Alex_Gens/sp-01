package ru.kazakov.iteco.context;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.endpoint.ISessionEndpoint;
import ru.kazakov.iteco.api.endpoint.IUserEndpoint;
import ru.kazakov.iteco.api.endpoint.RoleType;
import ru.kazakov.iteco.api.endpoint.UserDTO;
import ru.kazakov.iteco.api.service.ITerminalService;
import ru.kazakov.iteco.command.AbstractCommand;
import ru.kazakov.iteco.util.Password;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public final class Bootstrap implements CurrentState {

    @NotNull
    @Autowired
    private List<AbstractCommand> commandList;

    @NotNull
    private Map<String, AbstractCommand> commands;

    @NotNull
    @Autowired
    private ISessionEndpoint sessionEndpoint;

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @Getter
    @NotNull
    @Autowired
    private ITerminalService terminalService;

    @Getter
    @Setter
    @Nullable
    private String token = null;

    public void init() {
        try {
            registry();
         //   addUsers();
            start();
        }
//        catch (InstantiationException |
//                InvocationTargetException |
//                 NoSuchMethodException |
//                 IllegalAccessException e) {e.getMessage();}
          catch (Exception e) {e.printStackTrace();}
    }

    private void start() {
        terminalService.write("*** WELCOME TO TASK MANAGER ***");
        @NotNull String command = "";
        try {
            while (true) {
                if (token == null || token.isEmpty()) {
                    terminalService.write("You are not authorized. Use \"user-login\" for authorization.");
                    terminalService.separateLines();
                }
                command = terminalService.enterIgnoreEmpty();
                command = command.trim().toLowerCase();
                execute(command);
            }
        } catch (Exception e) {e.printStackTrace();}
    }

    @NotNull
    public List<AbstractCommand> getCommands() {return new ArrayList<>(commands.values());}

    private void registry() {
        commands = commandList.stream().filter(v -> !v.getClass().getName().contains("abstract"))
                .collect(Collectors.toMap(AbstractCommand::getName, v -> v));
    }

    private void execute(@Nullable final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        if (!commands.containsKey(command)) {
            terminalService.write("Command \"" + command +  "\" doesn't exist. Use \"help\" to show all commands.");
            return;
        }
        @NotNull final AbstractCommand abstractCommand = commands.get(command);
        if (token == null && abstractCommand.getRoleType() == RoleType.ADMINISTRATOR) {
            terminalService.write("Command \"" + command +  "\" doesn't exist. Use \"help\" to show all commands.");
            return;
        }
        if ((token == null || token.isEmpty()) && abstractCommand.getRoleType() == RoleType.DEFAULT) {
            terminalService.write("[NO ACCESS]");
            return;
        }
        if ((token == null || token.isEmpty()) && abstractCommand.getRoleType() == RoleType.NONE) {
            abstractCommand.execute();
            return;
        }
        @Nullable final RoleType currentUserRoleType = sessionEndpoint.getSessionRoleType(token);
        if (currentUserRoleType == null) {
            terminalService.write("[NO ACCESS]");
            return;
        }
        if (currentUserRoleType !=  RoleType.ADMINISTRATOR && abstractCommand.getRoleType() == RoleType.ADMINISTRATOR) {
            terminalService.write("Command \"" + command +  "\" doesn't exist. Use \"help\" to show all commands.");
            terminalService.separateLines();
            return;
        }
        abstractCommand.execute();
    }

    private void addUsers() throws Exception{
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin("user");
        @NotNull final String password = Password.getHashedPassword("pass");
        user.setPassword(password);
        user.setRoleType(RoleType.DEFAULT);
        @NotNull final UserDTO admin = new UserDTO();
        admin.setLogin("admin");
        admin.setPassword(password);
        admin.setRoleType(RoleType.ADMINISTRATOR);
        userEndpoint.persistUser(user);
        userEndpoint.persistUser(admin);
    }

}
