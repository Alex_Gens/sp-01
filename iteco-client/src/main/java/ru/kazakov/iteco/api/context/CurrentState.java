package ru.kazakov.iteco.api.context;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.command.AbstractCommand;
import java.util.List;

public interface CurrentState {

    @Nullable
    public String getToken();

    public void setToken(@Nullable final String token);

    @NotNull
    public List<AbstractCommand> getCommands();

}
