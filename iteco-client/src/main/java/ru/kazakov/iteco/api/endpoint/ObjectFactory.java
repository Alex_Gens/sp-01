
package ru.kazakov.iteco.api.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.kazakov.iteco.api.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://endpoint.api.iteco.kazakov.ru/", "Exception");
    private final static QName _ExistDomain_QNAME = new QName("http://endpoint.api.iteco.kazakov.ru/", "existDomain");
    private final static QName _ExistDomainResponse_QNAME = new QName("http://endpoint.api.iteco.kazakov.ru/", "existDomainResponse");
    private final static QName _IsDomainDirectory_QNAME = new QName("http://endpoint.api.iteco.kazakov.ru/", "isDomainDirectory");
    private final static QName _IsDomainDirectoryResponse_QNAME = new QName("http://endpoint.api.iteco.kazakov.ru/", "isDomainDirectoryResponse");
    private final static QName _LoadDomainBin_QNAME = new QName("http://endpoint.api.iteco.kazakov.ru/", "loadDomainBin");
    private final static QName _LoadDomainBinResponse_QNAME = new QName("http://endpoint.api.iteco.kazakov.ru/", "loadDomainBinResponse");
    private final static QName _LoadDomainFasterJson_QNAME = new QName("http://endpoint.api.iteco.kazakov.ru/", "loadDomainFasterJson");
    private final static QName _LoadDomainFasterJsonResponse_QNAME = new QName("http://endpoint.api.iteco.kazakov.ru/", "loadDomainFasterJsonResponse");
    private final static QName _LoadDomainFasterXml_QNAME = new QName("http://endpoint.api.iteco.kazakov.ru/", "loadDomainFasterXml");
    private final static QName _LoadDomainFasterXmlResponse_QNAME = new QName("http://endpoint.api.iteco.kazakov.ru/", "loadDomainFasterXmlResponse");
    private final static QName _LoadDomainJaxbJson_QNAME = new QName("http://endpoint.api.iteco.kazakov.ru/", "loadDomainJaxbJson");
    private final static QName _LoadDomainJaxbJsonResponse_QNAME = new QName("http://endpoint.api.iteco.kazakov.ru/", "loadDomainJaxbJsonResponse");
    private final static QName _LoadDomainJaxbXml_QNAME = new QName("http://endpoint.api.iteco.kazakov.ru/", "loadDomainJaxbXml");
    private final static QName _LoadDomainJaxbXmlResponse_QNAME = new QName("http://endpoint.api.iteco.kazakov.ru/", "loadDomainJaxbXmlResponse");
    private final static QName _SaveDomainBin_QNAME = new QName("http://endpoint.api.iteco.kazakov.ru/", "saveDomainBin");
    private final static QName _SaveDomainBinResponse_QNAME = new QName("http://endpoint.api.iteco.kazakov.ru/", "saveDomainBinResponse");
    private final static QName _SaveDomainFasterJson_QNAME = new QName("http://endpoint.api.iteco.kazakov.ru/", "saveDomainFasterJson");
    private final static QName _SaveDomainFasterJsonResponse_QNAME = new QName("http://endpoint.api.iteco.kazakov.ru/", "saveDomainFasterJsonResponse");
    private final static QName _SaveDomainFasterXml_QNAME = new QName("http://endpoint.api.iteco.kazakov.ru/", "saveDomainFasterXml");
    private final static QName _SaveDomainFasterXmlResponse_QNAME = new QName("http://endpoint.api.iteco.kazakov.ru/", "saveDomainFasterXmlResponse");
    private final static QName _SaveDomainJaxbJson_QNAME = new QName("http://endpoint.api.iteco.kazakov.ru/", "saveDomainJaxbJson");
    private final static QName _SaveDomainJaxbJsonResponse_QNAME = new QName("http://endpoint.api.iteco.kazakov.ru/", "saveDomainJaxbJsonResponse");
    private final static QName _SaveDomainJaxbXml_QNAME = new QName("http://endpoint.api.iteco.kazakov.ru/", "saveDomainJaxbXml");
    private final static QName _SaveDomainJaxbXmlResponse_QNAME = new QName("http://endpoint.api.iteco.kazakov.ru/", "saveDomainJaxbXmlResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.kazakov.iteco.api.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link ExistDomain }
     * 
     */
    public ExistDomain createExistDomain() {
        return new ExistDomain();
    }

    /**
     * Create an instance of {@link ExistDomainResponse }
     * 
     */
    public ExistDomainResponse createExistDomainResponse() {
        return new ExistDomainResponse();
    }

    /**
     * Create an instance of {@link IsDomainDirectory }
     * 
     */
    public IsDomainDirectory createIsDomainDirectory() {
        return new IsDomainDirectory();
    }

    /**
     * Create an instance of {@link IsDomainDirectoryResponse }
     * 
     */
    public IsDomainDirectoryResponse createIsDomainDirectoryResponse() {
        return new IsDomainDirectoryResponse();
    }

    /**
     * Create an instance of {@link LoadDomainBin }
     * 
     */
    public LoadDomainBin createLoadDomainBin() {
        return new LoadDomainBin();
    }

    /**
     * Create an instance of {@link LoadDomainBinResponse }
     * 
     */
    public LoadDomainBinResponse createLoadDomainBinResponse() {
        return new LoadDomainBinResponse();
    }

    /**
     * Create an instance of {@link LoadDomainFasterJson }
     * 
     */
    public LoadDomainFasterJson createLoadDomainFasterJson() {
        return new LoadDomainFasterJson();
    }

    /**
     * Create an instance of {@link LoadDomainFasterJsonResponse }
     * 
     */
    public LoadDomainFasterJsonResponse createLoadDomainFasterJsonResponse() {
        return new LoadDomainFasterJsonResponse();
    }

    /**
     * Create an instance of {@link LoadDomainFasterXml }
     * 
     */
    public LoadDomainFasterXml createLoadDomainFasterXml() {
        return new LoadDomainFasterXml();
    }

    /**
     * Create an instance of {@link LoadDomainFasterXmlResponse }
     * 
     */
    public LoadDomainFasterXmlResponse createLoadDomainFasterXmlResponse() {
        return new LoadDomainFasterXmlResponse();
    }

    /**
     * Create an instance of {@link LoadDomainJaxbJson }
     * 
     */
    public LoadDomainJaxbJson createLoadDomainJaxbJson() {
        return new LoadDomainJaxbJson();
    }

    /**
     * Create an instance of {@link LoadDomainJaxbJsonResponse }
     * 
     */
    public LoadDomainJaxbJsonResponse createLoadDomainJaxbJsonResponse() {
        return new LoadDomainJaxbJsonResponse();
    }

    /**
     * Create an instance of {@link LoadDomainJaxbXml }
     * 
     */
    public LoadDomainJaxbXml createLoadDomainJaxbXml() {
        return new LoadDomainJaxbXml();
    }

    /**
     * Create an instance of {@link LoadDomainJaxbXmlResponse }
     * 
     */
    public LoadDomainJaxbXmlResponse createLoadDomainJaxbXmlResponse() {
        return new LoadDomainJaxbXmlResponse();
    }

    /**
     * Create an instance of {@link SaveDomainBin }
     * 
     */
    public SaveDomainBin createSaveDomainBin() {
        return new SaveDomainBin();
    }

    /**
     * Create an instance of {@link SaveDomainBinResponse }
     * 
     */
    public SaveDomainBinResponse createSaveDomainBinResponse() {
        return new SaveDomainBinResponse();
    }

    /**
     * Create an instance of {@link SaveDomainFasterJson }
     * 
     */
    public SaveDomainFasterJson createSaveDomainFasterJson() {
        return new SaveDomainFasterJson();
    }

    /**
     * Create an instance of {@link SaveDomainFasterJsonResponse }
     * 
     */
    public SaveDomainFasterJsonResponse createSaveDomainFasterJsonResponse() {
        return new SaveDomainFasterJsonResponse();
    }

    /**
     * Create an instance of {@link SaveDomainFasterXml }
     * 
     */
    public SaveDomainFasterXml createSaveDomainFasterXml() {
        return new SaveDomainFasterXml();
    }

    /**
     * Create an instance of {@link SaveDomainFasterXmlResponse }
     * 
     */
    public SaveDomainFasterXmlResponse createSaveDomainFasterXmlResponse() {
        return new SaveDomainFasterXmlResponse();
    }

    /**
     * Create an instance of {@link SaveDomainJaxbJson }
     * 
     */
    public SaveDomainJaxbJson createSaveDomainJaxbJson() {
        return new SaveDomainJaxbJson();
    }

    /**
     * Create an instance of {@link SaveDomainJaxbJsonResponse }
     * 
     */
    public SaveDomainJaxbJsonResponse createSaveDomainJaxbJsonResponse() {
        return new SaveDomainJaxbJsonResponse();
    }

    /**
     * Create an instance of {@link SaveDomainJaxbXml }
     * 
     */
    public SaveDomainJaxbXml createSaveDomainJaxbXml() {
        return new SaveDomainJaxbXml();
    }

    /**
     * Create an instance of {@link SaveDomainJaxbXmlResponse }
     * 
     */
    public SaveDomainJaxbXmlResponse createSaveDomainJaxbXmlResponse() {
        return new SaveDomainJaxbXmlResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.iteco.kazakov.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExistDomain }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.iteco.kazakov.ru/", name = "existDomain")
    public JAXBElement<ExistDomain> createExistDomain(ExistDomain value) {
        return new JAXBElement<ExistDomain>(_ExistDomain_QNAME, ExistDomain.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExistDomainResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.iteco.kazakov.ru/", name = "existDomainResponse")
    public JAXBElement<ExistDomainResponse> createExistDomainResponse(ExistDomainResponse value) {
        return new JAXBElement<ExistDomainResponse>(_ExistDomainResponse_QNAME, ExistDomainResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsDomainDirectory }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.iteco.kazakov.ru/", name = "isDomainDirectory")
    public JAXBElement<IsDomainDirectory> createIsDomainDirectory(IsDomainDirectory value) {
        return new JAXBElement<IsDomainDirectory>(_IsDomainDirectory_QNAME, IsDomainDirectory.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsDomainDirectoryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.iteco.kazakov.ru/", name = "isDomainDirectoryResponse")
    public JAXBElement<IsDomainDirectoryResponse> createIsDomainDirectoryResponse(IsDomainDirectoryResponse value) {
        return new JAXBElement<IsDomainDirectoryResponse>(_IsDomainDirectoryResponse_QNAME, IsDomainDirectoryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDomainBin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.iteco.kazakov.ru/", name = "loadDomainBin")
    public JAXBElement<LoadDomainBin> createLoadDomainBin(LoadDomainBin value) {
        return new JAXBElement<LoadDomainBin>(_LoadDomainBin_QNAME, LoadDomainBin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDomainBinResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.iteco.kazakov.ru/", name = "loadDomainBinResponse")
    public JAXBElement<LoadDomainBinResponse> createLoadDomainBinResponse(LoadDomainBinResponse value) {
        return new JAXBElement<LoadDomainBinResponse>(_LoadDomainBinResponse_QNAME, LoadDomainBinResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDomainFasterJson }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.iteco.kazakov.ru/", name = "loadDomainFasterJson")
    public JAXBElement<LoadDomainFasterJson> createLoadDomainFasterJson(LoadDomainFasterJson value) {
        return new JAXBElement<LoadDomainFasterJson>(_LoadDomainFasterJson_QNAME, LoadDomainFasterJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDomainFasterJsonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.iteco.kazakov.ru/", name = "loadDomainFasterJsonResponse")
    public JAXBElement<LoadDomainFasterJsonResponse> createLoadDomainFasterJsonResponse(LoadDomainFasterJsonResponse value) {
        return new JAXBElement<LoadDomainFasterJsonResponse>(_LoadDomainFasterJsonResponse_QNAME, LoadDomainFasterJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDomainFasterXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.iteco.kazakov.ru/", name = "loadDomainFasterXml")
    public JAXBElement<LoadDomainFasterXml> createLoadDomainFasterXml(LoadDomainFasterXml value) {
        return new JAXBElement<LoadDomainFasterXml>(_LoadDomainFasterXml_QNAME, LoadDomainFasterXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDomainFasterXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.iteco.kazakov.ru/", name = "loadDomainFasterXmlResponse")
    public JAXBElement<LoadDomainFasterXmlResponse> createLoadDomainFasterXmlResponse(LoadDomainFasterXmlResponse value) {
        return new JAXBElement<LoadDomainFasterXmlResponse>(_LoadDomainFasterXmlResponse_QNAME, LoadDomainFasterXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDomainJaxbJson }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.iteco.kazakov.ru/", name = "loadDomainJaxbJson")
    public JAXBElement<LoadDomainJaxbJson> createLoadDomainJaxbJson(LoadDomainJaxbJson value) {
        return new JAXBElement<LoadDomainJaxbJson>(_LoadDomainJaxbJson_QNAME, LoadDomainJaxbJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDomainJaxbJsonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.iteco.kazakov.ru/", name = "loadDomainJaxbJsonResponse")
    public JAXBElement<LoadDomainJaxbJsonResponse> createLoadDomainJaxbJsonResponse(LoadDomainJaxbJsonResponse value) {
        return new JAXBElement<LoadDomainJaxbJsonResponse>(_LoadDomainJaxbJsonResponse_QNAME, LoadDomainJaxbJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDomainJaxbXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.iteco.kazakov.ru/", name = "loadDomainJaxbXml")
    public JAXBElement<LoadDomainJaxbXml> createLoadDomainJaxbXml(LoadDomainJaxbXml value) {
        return new JAXBElement<LoadDomainJaxbXml>(_LoadDomainJaxbXml_QNAME, LoadDomainJaxbXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDomainJaxbXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.iteco.kazakov.ru/", name = "loadDomainJaxbXmlResponse")
    public JAXBElement<LoadDomainJaxbXmlResponse> createLoadDomainJaxbXmlResponse(LoadDomainJaxbXmlResponse value) {
        return new JAXBElement<LoadDomainJaxbXmlResponse>(_LoadDomainJaxbXmlResponse_QNAME, LoadDomainJaxbXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDomainBin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.iteco.kazakov.ru/", name = "saveDomainBin")
    public JAXBElement<SaveDomainBin> createSaveDomainBin(SaveDomainBin value) {
        return new JAXBElement<SaveDomainBin>(_SaveDomainBin_QNAME, SaveDomainBin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDomainBinResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.iteco.kazakov.ru/", name = "saveDomainBinResponse")
    public JAXBElement<SaveDomainBinResponse> createSaveDomainBinResponse(SaveDomainBinResponse value) {
        return new JAXBElement<SaveDomainBinResponse>(_SaveDomainBinResponse_QNAME, SaveDomainBinResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDomainFasterJson }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.iteco.kazakov.ru/", name = "saveDomainFasterJson")
    public JAXBElement<SaveDomainFasterJson> createSaveDomainFasterJson(SaveDomainFasterJson value) {
        return new JAXBElement<SaveDomainFasterJson>(_SaveDomainFasterJson_QNAME, SaveDomainFasterJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDomainFasterJsonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.iteco.kazakov.ru/", name = "saveDomainFasterJsonResponse")
    public JAXBElement<SaveDomainFasterJsonResponse> createSaveDomainFasterJsonResponse(SaveDomainFasterJsonResponse value) {
        return new JAXBElement<SaveDomainFasterJsonResponse>(_SaveDomainFasterJsonResponse_QNAME, SaveDomainFasterJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDomainFasterXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.iteco.kazakov.ru/", name = "saveDomainFasterXml")
    public JAXBElement<SaveDomainFasterXml> createSaveDomainFasterXml(SaveDomainFasterXml value) {
        return new JAXBElement<SaveDomainFasterXml>(_SaveDomainFasterXml_QNAME, SaveDomainFasterXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDomainFasterXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.iteco.kazakov.ru/", name = "saveDomainFasterXmlResponse")
    public JAXBElement<SaveDomainFasterXmlResponse> createSaveDomainFasterXmlResponse(SaveDomainFasterXmlResponse value) {
        return new JAXBElement<SaveDomainFasterXmlResponse>(_SaveDomainFasterXmlResponse_QNAME, SaveDomainFasterXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDomainJaxbJson }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.iteco.kazakov.ru/", name = "saveDomainJaxbJson")
    public JAXBElement<SaveDomainJaxbJson> createSaveDomainJaxbJson(SaveDomainJaxbJson value) {
        return new JAXBElement<SaveDomainJaxbJson>(_SaveDomainJaxbJson_QNAME, SaveDomainJaxbJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDomainJaxbJsonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.iteco.kazakov.ru/", name = "saveDomainJaxbJsonResponse")
    public JAXBElement<SaveDomainJaxbJsonResponse> createSaveDomainJaxbJsonResponse(SaveDomainJaxbJsonResponse value) {
        return new JAXBElement<SaveDomainJaxbJsonResponse>(_SaveDomainJaxbJsonResponse_QNAME, SaveDomainJaxbJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDomainJaxbXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.iteco.kazakov.ru/", name = "saveDomainJaxbXml")
    public JAXBElement<SaveDomainJaxbXml> createSaveDomainJaxbXml(SaveDomainJaxbXml value) {
        return new JAXBElement<SaveDomainJaxbXml>(_SaveDomainJaxbXml_QNAME, SaveDomainJaxbXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDomainJaxbXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.iteco.kazakov.ru/", name = "saveDomainJaxbXmlResponse")
    public JAXBElement<SaveDomainJaxbXmlResponse> createSaveDomainJaxbXmlResponse(SaveDomainJaxbXmlResponse value) {
        return new JAXBElement<SaveDomainJaxbXmlResponse>(_SaveDomainJaxbXmlResponse_QNAME, SaveDomainJaxbXmlResponse.class, null, value);
    }

}
