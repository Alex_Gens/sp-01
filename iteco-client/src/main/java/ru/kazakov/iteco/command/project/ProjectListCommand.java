package ru.kazakov.iteco.command.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kazakov.iteco.api.endpoint.SortType;
import java.util.*;

@Component
@NoArgsConstructor
public final class ProjectListCommand extends ProjectAbstractCommand {

    @Getter
    @NotNull
    private final String name = "project-list";

    @Getter
    @NotNull
    private final String description = "Show all projects.";

    @Override
    public void execute() throws Exception {
        if (currentState == null) throw new Exception();
        if (projectEndpoint == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final String token = currentState.getToken();
        if (token == null || token.isEmpty()) throw new Exception();
        if (projectEndpoint.isEmptyProjectRepositoryByCurrentId(token)) {
            terminalService.write("Project list is empty. Use \"project-create\" to create project.");
            terminalService.separateLines();
            return;
        }
        @NotNull final SortType[] sortTypes = SortType.values();
        Map<String, SortType> sortTypesMap = new TreeMap<>();
        for (int i = 0; i < sortTypes.length; i++) {
            sortTypesMap.put(String.valueOf(i + 1), sortTypes[i]);
        }
        terminalService.write("Select list sorting type");
        @NotNull String number;
        while (true) {
            sortTypesMap.forEach((k, v) -> terminalService.write(k + ". " + v));
            terminalService.separateLines();
            terminalService.write("ENTER SORTING TYPE NUMBER: ");
            number = terminalService.enterIgnoreEmpty().trim();
            if (!sortTypesMap.containsKey(number)) {
                terminalService.write("[NOT CORRECT]");
                terminalService.write("This number of sorting type doesn't exist!");
                terminalService.separateLines();
                continue;
            }
            terminalService.write("[CORRECT]");
            terminalService.separateLines();
            break;
        }
        @Nullable final List<String> projectsNames = projectEndpoint.findAllSortedProjectsByCurrentId(token, sortTypesMap.get(number));
        if (projectsNames == null) throw new Exception();
        terminalService.write("[PROJECTS LIST]");
        int counter = 1;
        for (String projectName : projectsNames) {
            terminalService.write(counter + ". " + projectName);
            counter++;
        }
        terminalService.separateLines();
    }

}
