package ru.kazakov.iteco.command.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.kazakov.iteco.api.endpoint.RoleType;
import ru.kazakov.iteco.api.endpoint.UserDTO;
import ru.kazakov.iteco.util.Password;

@Component
@NoArgsConstructor
public final class UserCreateCommand extends UserAbstractCommand {

    @Getter
    @NotNull
    private final String name = "user-create";

    @Getter
    @NotNull
    private final String description = "New user Registration.";

    @NotNull
    private RoleType roleType = RoleType.NONE;

    @NotNull
    @Override
    public RoleType getRoleType() {return this.roleType;}

    @Override
    public void execute() throws Exception {
        if (userEndpoint == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        terminalService.write("Registration new profile.");
        terminalService.write("ENTER LOGIN ");
        @NotNull String login = terminalService.enterIgnoreEmpty();
        if (userEndpoint.containsUser(login)) {
            terminalService.write("Login is already exist. Use another login.");
            terminalService.separateLines();
            return;
        }
        terminalService.write("[CORRECT]");
        terminalService.separateLines();
        @NotNull String firstPassword = "";
        @NotNull String secondPassword = "";
        while (true) {
            terminalService.write("ENTER PASSWORD: ");
            firstPassword = terminalService.enterIgnoreEmpty();
            terminalService.separateLines();
            terminalService.write("Confirm you password.");
            terminalService.write("ENTER PASSWORD: ");
            secondPassword = terminalService.enterIgnoreEmpty();
            if (!firstPassword.equals(secondPassword)) {
                terminalService.write("[NOT CORRECT]");
                terminalService.write("Entered passwords are different.");
                terminalService.separateLines();
                continue;
            }
            terminalService.write("[CORRECT]");
            terminalService.write("Profile created!");
            terminalService.separateLines();
            break;
        }
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        @NotNull final String password = Password.getHashedPassword(firstPassword);
        user.setPassword(password);
        userEndpoint.persistUser(user);
    }

}
