package ru.kazakov.iteco.command.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor
public final class ProjectClearCommand extends ProjectAbstractCommand {

    @Getter
    @NotNull
    private final String name = "project-clear";

    @Getter
    @NotNull
    private final String description = "Remove all projects.";

    @Override
    public void execute() throws Exception {
        if (currentState == null) throw new Exception();
        if (projectEndpoint == null) throw new Exception();
        if (taskEndpoint == null) throw new Exception();
        if (currentState.getToken() == null || currentState.getToken().isEmpty()) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final String token = currentState.getToken();
        if (token == null || token.isEmpty()) throw new Exception();
        projectEndpoint.removeAllProjectsByCurrentId(token);
        taskEndpoint.removeAllTasksWithProjects(token);
        terminalService.write("[ALL PROJECTS REMOVED]");
        terminalService.write("Projects successfully removed!");
        terminalService.separateLines();
    }

}
