package ru.kazakov.iteco.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kazakov.iteco.api.endpoint.ISessionEndpoint;
import ru.kazakov.iteco.api.endpoint.IUserEndpoint;
import ru.kazakov.iteco.api.endpoint.RoleType;
import ru.kazakov.iteco.command.AbstractCommand;

@Component
@NoArgsConstructor
public abstract class UserAbstractCommand extends AbstractCommand {

    @NotNull
    protected RoleType roleType = RoleType.DEFAULT;

    @NotNull
    @Autowired
    protected IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    protected ISessionEndpoint sessionEndpoint;

    @NotNull
    @Override
    public RoleType getRoleType() {return this.roleType;}

}
