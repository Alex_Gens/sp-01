package ru.kazakov.iteco.command.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kazakov.iteco.api.endpoint.SortType;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Component
@NoArgsConstructor
public final class TaskListCommand extends TaskAbstractCommand {

    @Getter
    @NotNull
    private final String name = "task-list";

    @Getter
    @NotNull
    private final String description = "Show all tasks.";

    @Override
    public void execute() throws Exception {
        if (currentState == null) throw new Exception();
        if (taskEndpoint == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final String token = currentState.getToken();
        if (token == null || token.isEmpty()) throw new Exception();
        if (taskEndpoint.isEmptyTaskRepositoryByCurrentId(token)) {
            terminalService.write("Task list is empty. Use \"task-create\" to create task.");
            terminalService.separateLines();
            return;
        }
        @NotNull final SortType[] sortTypes = SortType.values();
        Map<String, SortType> sortTypesMap = new TreeMap<>();
        for (int i = 0; i < sortTypes.length; i++) {
            sortTypesMap.put(String.valueOf(i + 1), sortTypes[i]);
        }
        terminalService.write("Select list sorting type");
        @NotNull String number;
        while (true) {
            sortTypesMap.forEach((k, v) -> terminalService.write(k + ". " + v.value()));
            terminalService.separateLines();
            terminalService.write("ENTER SORTING TYPE NUMBER: ");
            number = terminalService.enterIgnoreEmpty().trim();
            if (!sortTypesMap.containsKey(number)) {
                terminalService.write("[NOT CORRECT]");
                terminalService.write("This number of sorting type doesn't exist!");
                terminalService.separateLines();
                continue;
            }
            terminalService.write("[CORRECT]");
            terminalService.separateLines();
            break;
        }
        @Nullable final List<String> tasksNames = taskEndpoint.findAllSortedTasksByCurrentId(token, sortTypesMap.get(number));
        if (tasksNames == null) throw new Exception();
        terminalService.write("[TASKS LIST]");
        int counter = 1;
        for (String taskName : tasksNames) {
            terminalService.write(counter + ". " + taskName);
            counter++;
        }
        terminalService.separateLines();
    }

}
