package ru.kazakov.iteco.command.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor
public final class ProjectCreateCommand extends ProjectAbstractCommand {

    @Getter
    @NotNull
    private final String name = "project-create";

    @Getter
    @NotNull
    private final String description = "Create new project.";

    @Override
    public void execute() throws Exception {
        if (currentState == null) throw new Exception();
        if (projectEndpoint == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final String token = currentState.getToken();
        if (token == null || token.isEmpty()) throw new Exception();
        terminalService.write("ENTER PROJECT NAME: ");
        @NotNull final String name = terminalService.enterIgnoreEmpty().trim();
        if (projectEndpoint.containsProjectByCurrentId(token, name)) {
            terminalService.write("[NOT CREATED]");
            terminalService.write("Project with this name is already exists. Use another project name.");
            terminalService.separateLines();
            return;
        }
        projectEndpoint.createProject(token, name);
        terminalService.write("[CREATED]");
        terminalService.write("Project successfully created!");
        terminalService.separateLines();
    }

}
