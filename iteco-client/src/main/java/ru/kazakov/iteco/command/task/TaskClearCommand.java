package ru.kazakov.iteco.command.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor
public final class TaskClearCommand extends TaskAbstractCommand {

    @Getter
    @NotNull
    private final String name = "task-clear";

    @Getter
    @NotNull
    private final String description = "Remove all tasks.";

    @Override
    public void execute() throws Exception {
        if (currentState == null) throw new Exception();
        if (taskEndpoint == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final String token = currentState.getToken();
        if (token == null || token.isEmpty()) throw new Exception();
        taskEndpoint.removeAllTasksByCurrentId(token);
        terminalService.write("[ALL TASKS REMOVED]");
        terminalService.write("Tasks successfully removed!");
        terminalService.separateLines();
    }

}
