package ru.kazakov.iteco.command.data.save;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor
public final class DataSaveJaxbJsonCommand extends DataAbstractSaveCommand {

    @Getter
    @NotNull
    private final String name = "data-save-jaxb-json";

    @Getter
    @NotNull
    private final String description = "Save data in json by JAX-B.";

    @Override
    public void execute() throws Exception {
        if (terminalService == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (domainEndpoint == null) throw new Exception();
        @Nullable final String token = currentState.getToken();
        if (token == null || token.isEmpty()) throw new Exception();
        if (!confirmed()) return;
        @NotNull final String fileName = "jaxb.json";
        domainEndpoint.saveDomainJaxbJson(token, directory, fileName);
        terminalService.write("[SAVED]");
        terminalService.write("Data successfully saved in " + fileName + "!");
        terminalService.separateLines();
    }

}
