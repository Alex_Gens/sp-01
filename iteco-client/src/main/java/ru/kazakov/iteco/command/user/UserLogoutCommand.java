package ru.kazakov.iteco.command.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kazakov.iteco.api.endpoint.UserDTO;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.GregorianCalendar;

@Component
@NoArgsConstructor
public final class UserLogoutCommand extends UserAbstractCommand {

    @Getter
    @NotNull
    private final String name = "user-logout";

    @Getter
    @NotNull
    private final String description = "Logout from account.";

    @Override
    public void execute() throws Exception {
        if (currentState == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final String token = currentState.getToken();
        if (token == null || token.isEmpty()) throw new Exception();
        @Nullable final UserDTO user = userEndpoint.findCurrentUser(token);
        if (user == null) throw new Exception();
        @NotNull final GregorianCalendar temp = new GregorianCalendar();
        temp.setTime(new Date(System.currentTimeMillis()));
        @NotNull final XMLGregorianCalendar calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(temp);
        user.setDateFinish(calendar);
        userEndpoint.mergeUser(token, user);
        sessionEndpoint.removeSessionByUserId(user.getId());
        currentState.setToken(null);
        terminalService.write("[LOGOUT]");
        terminalService.write("You logout from your account.");
        terminalService.separateLines();
    }

}
