package ru.kazakov.iteco.command.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kazakov.iteco.api.endpoint.ISessionEndpoint;
import ru.kazakov.iteco.api.endpoint.RoleType;
import ru.kazakov.iteco.api.endpoint.UserDTO;
import ru.kazakov.iteco.util.Password;

@Component
@NoArgsConstructor
public final class UserChangePasswordCommand extends UserAbstractCommand {

    @Getter
    @NotNull
    private final String name = "user-change-password";

    @Getter
    @NotNull
    private final String description = "Change profile password.";

    @NotNull
    @Autowired
    private ISessionEndpoint sessionEndpoint;

    @Override
    public void execute() throws Exception {
        if (currentState == null) throw new Exception();
        if (userEndpoint == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final String token = currentState.getToken();
        if (token == null || token.isEmpty()) throw new Exception();
        @NotNull final RoleType currentUserRoleType = sessionEndpoint.getSessionRoleType(token);
        if (currentUserRoleType == null) throw new Exception();
        @Nullable UserDTO user = null;
        if (currentUserRoleType == RoleType.ADMINISTRATOR) {
            terminalService.write("Enter user's login to change profile password.   [" +
                    currentUserRoleType + "]");
            terminalService.write("ENTER LOGIN: ");
            @NotNull final String login = terminalService.enterIgnoreEmpty();
            boolean isExist = userEndpoint.containsUser(login);
            if (!isExist) {
                terminalService.write("[NOT CORRECT]");
                terminalService.write("User with that login doesn't exist.");
                terminalService.separateLines();
                return;
            }
            user = userEndpoint.findUserByLogin(token, login);
            if (user == null) throw new Exception();
        }

        if (currentUserRoleType != RoleType.ADMINISTRATOR) {
            user = userEndpoint.findCurrentUser(token);
            checkOldPassword(user);
        }
        @NotNull String firstPassword = "";
        @NotNull String secondPassword = "";
        while (true) {
            terminalService.write("ENTER NEW PASSWORD: ");
            firstPassword = terminalService.enterIgnoreEmpty();
            terminalService.write("Confirm you password.");
            terminalService.write("ENTER NEW PASSWORD: ");
            secondPassword = terminalService.enterIgnoreEmpty();
            if (!firstPassword.equals(secondPassword)) {
                terminalService.write("[NOT UPDATED]");
                terminalService.write("Entered passwords are different.");
                terminalService.separateLines();
                continue;
            }
            terminalService.write("[UPDATED]");
            break;
        }
        @NotNull final String newPassword = Password.getHashedPassword(firstPassword);
        user.setPassword(newPassword);
        userEndpoint.mergeUser(token, user);
        terminalService.write("Password updated!");
        terminalService.separateLines();
    }

    private void checkOldPassword(@Nullable final UserDTO user) throws Exception {
        if (user == null) throw new Exception();
        if (user.getPassword() == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        terminalService.write("ENTER PASSWORD: ");
        @NotNull final String entered = terminalService.enterIgnoreEmpty();
        @NotNull final String enteredPassword = Password.getHashedPassword(entered);
        @NotNull final String oldPassword = user.getPassword();
        if (!enteredPassword.equals(oldPassword)) {
            terminalService.write("[NOT CORRECT]");
            terminalService.separateLines();
        }
    }

}
