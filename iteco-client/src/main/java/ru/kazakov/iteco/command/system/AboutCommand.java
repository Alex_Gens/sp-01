package ru.kazakov.iteco.command.system;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.kazakov.iteco.api.endpoint.RoleType;
import ru.kazakov.iteco.command.AbstractCommand;

@Component
@NoArgsConstructor
public final class AboutCommand extends AbstractCommand {

    @Getter
    @NotNull
    private final String name = "about";

    @Getter
    @NotNull
    private final String description = "Show information about build.";

    @NotNull
    private RoleType roleType = RoleType.NONE;

    @NotNull
    @Override
    public RoleType getRoleType() {return this.roleType;}

    @Override
    public void execute() throws Exception {
        if (terminalService == null) throw new Exception();
        terminalService.write("Build number is: " + Manifests.read("buildNumber"));
        terminalService.write("Developer: " + Manifests.read("developer"));
        terminalService.separateLines();
    }

}
