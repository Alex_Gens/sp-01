package ru.kazakov.iteco.command.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kazakov.iteco.api.endpoint.ProjectDTO;
import ru.kazakov.iteco.api.endpoint.TaskDTO;

@Component
@NoArgsConstructor
public final class ProjectAddTaskCommand extends ProjectAbstractCommand {

    @Getter
    @NotNull
    private final String name = "project-add-task";

    @Getter
    @NotNull
    private final String description = "Add task to project.";

    @Override
    public void execute() throws Exception {
        if (currentState == null) throw new Exception();
        if (projectEndpoint == null) throw new Exception();
        if (taskEndpoint == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final String token = currentState.getToken();
        if (token == null || token.isEmpty()) throw new Exception();
        @Nullable final ProjectDTO project = getProjectByPart();
        if (project == null) return;
        @Nullable final TaskDTO task = getTaskByPart();
        if (task == null) return;
        if (task.getProjectId() == null || !task.getProjectId().equals(project.getId())) {
            task.setProjectId(project.getId());
            taskEndpoint.mergeTask(token, task);
            terminalService.write("Task successfully added!");
            terminalService.separateLines();
            return;
        }
        terminalService.write("Task is already added. Use project-list-tasks to see tasks in project.");
        terminalService.separateLines();
    }

}
