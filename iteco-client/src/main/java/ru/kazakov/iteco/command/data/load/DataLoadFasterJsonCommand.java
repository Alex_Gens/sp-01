package ru.kazakov.iteco.command.data.load;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor
public final class DataLoadFasterJsonCommand extends DataAbstractLoadCommand {

    @Getter
    @NotNull
    private final String name = "data-load-faster-json";

    @Getter
    @NotNull
    private final String description = "Load data in json by FasterXml.";

    @Override
    public void execute() throws Exception {
        if (terminalService == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (domainEndpoint == null) throw new Exception();
        @Nullable final String token = currentState.getToken();
        if (token == null || token.isEmpty()) throw new Exception();
        if (!confirmed()) return;
        @NotNull final String fileName = "faster.json";
        if (!domainEndpoint.isDomainDirectory(token, directory)) {
            terminalService.write("[NOT LOADED]");
            terminalService.write("File not exist.");
            terminalService.separateLines();
        }
        if (!domainEndpoint.existDomain(token, directory, fileName)) {
            terminalService.write("[NOT LOADED]");
            terminalService.write("File not exist.");
            terminalService.separateLines();
            return;
        }
        domainEndpoint.loadDomainFasterJson(token, directory, fileName);
        currentState.setToken(null);
        terminalService.write("[LOADED]");
        terminalService.write("All data successfully loaded!");
        terminalService.separateLines();
    }

}
