package ru.kazakov.iteco.command.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kazakov.iteco.api.endpoint.RoleType;
import ru.kazakov.iteco.api.endpoint.UserDTO;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
@NoArgsConstructor
public final class UserGetCommand extends UserAbstractCommand {

    @Getter
    @NotNull
    private final String name = "user-get";

    @Getter
    @NotNull
    private final String description = "Show user profile information.";

    @Override
    public void execute() throws Exception {
        if (currentState == null) throw new Exception();
        if (userEndpoint == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final String token = currentState.getToken();
        if (token == null || token.isEmpty()) throw new Exception();
        @NotNull final RoleType currentUserRoleType = sessionEndpoint.getSessionRoleType(token);
        if (currentUserRoleType == null) throw new Exception();
        @Nullable UserDTO user = null;
        if (currentUserRoleType == RoleType.ADMINISTRATOR) {
            terminalService.write("Enter user's login to change profile password.   [" +
                    currentUserRoleType + "]");
            terminalService.write("ENTER LOGIN: ");
            @NotNull final String login = terminalService.enterIgnoreEmpty();
            boolean isExist = userEndpoint.containsUser(login);
            if (!isExist) {
                terminalService.write("[NOT CORRECT]");
                terminalService.write("User with that login doesn't exist.");
                terminalService.separateLines();
                return;
            }
            user = userEndpoint.findUserByLogin(token, login);
            if (user == null) throw new Exception();
        }
        if (currentUserRoleType != RoleType.ADMINISTRATOR) {
            user = userEndpoint.findCurrentUser(token);
        }
        terminalService.write("Profile information: ");
        @Nullable final String name = user.getName();
        boolean nameIsExist = name != null && !name.isEmpty();
        if (nameIsExist) terminalService.write("Name: " + name);
        @Nullable final String login = user.getLogin();
        final boolean loginIsExist = login != null && !login.isEmpty();
        if (loginIsExist) terminalService.write("Login: " + login);
        if (!nameIsExist && !loginIsExist) {
            terminalService.write("Profile has no information.");
            terminalService.separateLines();
            return;
        }
        @Nullable final XMLGregorianCalendar gregory = user.getDateCreate();
        if (gregory == null) return;
        @NotNull final Date dateStart = gregory.toGregorianCalendar().getTime();
        @NotNull final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        terminalService.write("Registration date: " + dateFormat.format(dateStart));
        terminalService.separateLines();
    }

}
