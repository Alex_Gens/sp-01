package ru.kazakov.iteco.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kazakov.iteco.api.context.CurrentState;
import ru.kazakov.iteco.api.endpoint.RoleType;
import ru.kazakov.iteco.api.service.ITerminalService;

@Component
@NoArgsConstructor
public abstract class AbstractCommand {

    @Nullable
    @Autowired
    protected ITerminalService terminalService;

    @Nullable
    @Autowired
    protected CurrentState currentState;

    @NotNull
    protected RoleType roleType = RoleType.DEFAULT;

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    public abstract void execute() throws Exception;

    @NotNull
    public RoleType getRoleType() {return this.roleType;}

}
