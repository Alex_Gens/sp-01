package ru.kazakov.iteco.command.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kazakov.iteco.api.endpoint.ProjectDTO;

@Component
@NoArgsConstructor
public final class ProjectRemoveCommand extends ProjectAbstractCommand {

    @Getter
    @NotNull
    private final String name = "project-remove";

    @Getter
    @NotNull
    private final String description = "Remove project.";

    @Override
    public void execute() throws Exception {
        if (currentState == null) throw new Exception();
        if (projectEndpoint == null) throw new Exception();
        if (taskEndpoint == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final String token = currentState.getToken();
        if (token == null || token.isEmpty()) throw new Exception();
        @Nullable final ProjectDTO project = getProjectByPart();
        if (project == null) return;
        taskEndpoint.removeTasksWithProject(token, project.getId());
        projectEndpoint.removeProjectById(token, project.getId());
        terminalService.write("[REMOVED]");
        terminalService.write("Project successfully removed!");
        terminalService.separateLines();
    }

}
