package ru.kazakov.iteco.command.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kazakov.iteco.api.endpoint.ProjectDTO;
import ru.kazakov.iteco.api.endpoint.Status;
import ru.kazakov.iteco.util.DateUtil;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.*;

@Component
@NoArgsConstructor
public final class ProjectUpdateCommand extends ProjectAbstractCommand {

    @Getter
    @NotNull
    private final String name = "project-update";

    @Getter
    @NotNull
    private final String description = "Update project information.";

    @Override
    public void execute() throws Exception {
        if (currentState == null) throw new Exception();
        if (projectEndpoint == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final String token = currentState.getToken();
        if (token == null || token.isEmpty()) throw new Exception();
        @Nullable ProjectDTO project = getProjectByPart();
        if (project == null) return;
        Map<String, String> items = new HashMap<>();
        items.put(String.valueOf(1), "Project start date");
        items.put(String.valueOf(2), "Project finish date");
        items.put(String.valueOf(3), "Project status");
        items.put(String.valueOf(4), "Project information");
        @NotNull String number;
        while (true) {
            terminalService.write("Select item to update: ");
            items.forEach((k, v) -> terminalService.write(k + ". " + v));
            terminalService.separateLines();
            terminalService.write("ENTER ITEM NUMBER: ");
            number = terminalService.enterIgnoreEmpty().trim();
            if (!items.containsKey(number)) {
                terminalService.write("[NOT CORRECT]");
                terminalService.write("This number of item doesn't exist!");
                terminalService.separateLines();
                continue;
            }
            terminalService.write("[CORRECT]");
            terminalService.separateLines();
            break;
        }

        switch (number) {
            case "1" : project = updateDateStart(project); break;
            case "2" : project = updateDateFinish(project); break;
            case "3" : project = updateStatus(project); break;
            case "4" : project = updateInformation(project);
        }
        projectEndpoint.mergeProject(token, project);
        terminalService.write("[UPDATED]");
        terminalService.write("Project successfully updated!");
        terminalService.separateLines();
    }

    private ProjectDTO updateDateStart(@NotNull final ProjectDTO project) throws Exception {
        if (terminalService == null) throw new Exception();
        while (true) {
            terminalService.write("Enter new project's start date in dd.mm.yyyy format.");
            terminalService.write("ENTER START DATE:");
            @NotNull final String enteredDate = terminalService.enterIgnoreEmpty();
            @Nullable final Date date = DateUtil.parseDate(enteredDate);
            if (date == null) {
                terminalService.write("[NOT UPDATED]");
                terminalService.write("Incorrect date or format.");
                terminalService.separateLines();
                continue;
        }
            @NotNull final GregorianCalendar temp = new GregorianCalendar();
            temp.setTime(date);
            @NotNull final XMLGregorianCalendar calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(temp);
            project.setDateStart(calendar);
            return project;
        }
    }


    private ProjectDTO updateDateFinish(@NotNull final ProjectDTO project) throws Exception {
        if (terminalService == null) throw new Exception();
        while (true) {
            terminalService.write("Enter new project's start date in dd.mm.yyyy format.");
            terminalService.write("ENTER FINISH DATE:");
            @NotNull final String enteredDate = terminalService.enterIgnoreEmpty();
            @Nullable final Date date = DateUtil.parseDate(enteredDate);
            if (date == null) {
                terminalService.write("[NOT UPDATED]");
                terminalService.write("Incorrect date or format.");
                terminalService.separateLines();
                continue;
            }
            @NotNull final GregorianCalendar temp = new GregorianCalendar();
            temp.setTime(date);
            @NotNull final XMLGregorianCalendar calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(temp);
            project.setDateFinish(calendar);
            return project;
        }
    }

    private ProjectDTO updateStatus(@NotNull final ProjectDTO project) throws Exception {
        if (terminalService == null) throw new Exception();
        @NotNull final Map<String, Status> statusMap = new TreeMap<>();
        @NotNull final Status[] statuses = Status.values();
        for (int i = 0; i < statuses.length; i++) {
            statusMap.put(String.valueOf(i + 1), statuses[i]);
        }
        terminalService.write("Select status:");
        @NotNull String number;
        while (true) {
            statusMap.forEach((k, v) -> terminalService.write(k + ". " + v.value()));
            terminalService.separateLines();
            terminalService.write("ENTER STATUS NUMBER: ");
            number = terminalService.enterIgnoreEmpty().trim();
            if (!statusMap.containsKey(number)) {
                terminalService.write("[NOT UPDATED]");
                terminalService.write("This number of status doesn't exist!");
                terminalService.separateLines();
                continue;
            }
            project.setStatus(statusMap.get(number));
            return project;
        }
    }

    private ProjectDTO updateInformation(@NotNull final ProjectDTO project) throws Exception {
        if (terminalService == null) throw new Exception();
        terminalService.write("Use \"-save\" to finish entering, and save information.");
        terminalService.write("ENTER PROJECT INFORMATION: ");
        @NotNull final String newInfo = terminalService.read();
        project.setInfo(newInfo);
        return project;
    }

}
