package ru.kazakov.iteco.command.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kazakov.iteco.api.endpoint.ProjectDTO;

@Component
@NoArgsConstructor
public final class ProjectGetCommand extends ProjectAbstractCommand {

    @Getter
    @NotNull
    private final String name = "project-get";

    @Getter
    @NotNull
    private final String description = "Show all project information.";

    @Override
    public void execute() throws Exception {
        if (currentState == null) throw new Exception();
        if (projectEndpoint == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final String token = currentState.getToken();
        if (token == null || token.isEmpty()) throw new Exception();
        @Nullable final ProjectDTO project = getProjectByPart();
        if (project == null) return;
        if (project.getInfo() == null || project.getInfo().isEmpty()) {
            terminalService.write("Project is empty. Use \"project-update\" to update this project.");
            terminalService.separateLines();
            return;
        }
        terminalService.write("[Project: " + project.getName() + "]");
        if (project.getInfo() == null) throw new Exception();
        terminalService.write(project.getInfo());
        terminalService.separateLines();
    }

}
