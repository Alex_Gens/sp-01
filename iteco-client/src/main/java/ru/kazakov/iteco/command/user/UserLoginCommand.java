package ru.kazakov.iteco.command.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kazakov.iteco.api.endpoint.RoleType;

@Component
@NoArgsConstructor
public final class UserLoginCommand extends UserAbstractCommand{

    private final boolean secure = false;

    @Getter
    @NotNull
    private final String name = "user-login";

    @Getter
    @NotNull
    private final String description = "User authorization.";

    @NotNull
    private RoleType roleType = RoleType.NONE;

    @NotNull
    @Override
    public RoleType getRoleType() {return this.roleType;}

    @Override
    public void execute() throws Exception {
        if (currentState == null) throw new Exception();
        if (userEndpoint == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        terminalService.write("ENTER LOGIN: ");
        @NotNull final String login = terminalService.enterIgnoreEmpty();
        terminalService.write("ENTER PASSWORD: ");
        @NotNull final String password = terminalService.enterIgnoreEmpty();
        if (!userEndpoint.containsUser(login)) {
            terminalService.write("[NOT CORRECT]");
            terminalService.write("User with that login or password doesn't exist.");
            terminalService.separateLines();
            return;
        }
        @Nullable final String token = sessionEndpoint.getInstanceToken(login, password);
        if (token == null) {
            terminalService.write("[NOT CORRECT]");
            terminalService.write("User with that login or password doesn't exist.");
            terminalService.separateLines();
            return;
        }
        terminalService.write("[CORRECT]");
        currentState.setToken(token);
        @NotNull final RoleType currentUserRoleType = sessionEndpoint.getSessionRoleType(token);
        if (currentUserRoleType == null) throw new Exception();
        terminalService.write("Welcome!" + " ["
                + currentUserRoleType + "]");
        terminalService.separateLines();
    }

}
